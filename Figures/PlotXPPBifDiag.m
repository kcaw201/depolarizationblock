% Plot bifurcation diagram from xppaut

close all; clear; clc;

filename = 'bifDiagG0p0Simple.dat';

data = load( filename);

figure(1);
hold on;

linewidth = 4.0;
markersize = 20.0;

branch_ids = unique( data(:,5));

for i = 1:length( branch_ids)
  
  ind = (data(:,5)==branch_ids(i));
  branch = data(ind,:);
  
  stab = (branch(:,4)==1) | (branch(:,4)==3);
  
  if branch_ids(i) == 1
  
    g(i) = plot( branch(stab,1), branch(stab,2), 'Linewidth', linewidth, 'Color', 'blue');
    k(i) = plot( branch(stab,1), branch(stab,3), 'Linewidth', linewidth, 'Color', 'blue');
    h(i) = plot( branch(~stab,1), branch(~stab,2), 'Linewidth', linewidth, 'Color', 'red');
    j(i) = plot( branch(~stab,1), branch(~stab,3), 'Linewidth', linewidth, 'Color', 'red');
    
  else
    
    if sum( stab) > 0
      g(i) = plot( branch(stab,1), branch(stab,2), 'Linestyle', 'none', 'Marker', '.', 'Markersize', markersize, 'Color', 'blue');
      k(i) = plot( branch(stab,1), branch(stab,3), 'Linestyle', 'none', 'Marker', '.', 'Markersize', markersize, 'Color', 'blue');
    end
    h(i) = plot( branch(~stab,1), branch(~stab,2), 'Linestyle', 'none', 'Marker', '.', 'Markersize', markersize, 'Color', 'red');
    j(i) = plot( branch(~stab,1), branch(~stab,3), 'Linestyle', 'none', 'Marker', '.', 'Markersize', markersize, 'Color', 'red');
    
  end

end

grid on;
xlabel( 'Current density (mA/cm^2)');
ylabel( 'Voltage (mV)');
set( gca, 'XLim', [-0.3,0.4], 'YLim', [-0.8,0.4], 'Fontsize', 20, ...
  'XTick', -0.3:0.1:0.3, 'YTick', -0.8:0.2:0.4);

name = strsplit( filename, '.');
fig_filename = strcat( name{1}, 'Xpp.eps');
saveas(1, fig_filename, 'epsc');