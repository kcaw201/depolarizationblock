% Script to make figures for persistent Na+ paper with Eric Wengert
% 11.8.2020
% Kyle Wedgwood

close all;
clear;

% Line settings
lw = 6;
ms = 60;

scale = 100.0;

folders = {'NowackiSimplifiedScaledFast1', 'NowackiSimplifiedFast2', ...
  'NowackiSimplifiedScaledSlow1', 'NowackiSimplifiedScaledSlow2'};

for i = 1:length(folders)
  folder_name = sprintf('~/Dropbox/MatCont7p2/Systems/%s/diagram', folders{i});
  addpath(folder_name);
end

%% First do 1D bifurcation analysis
hfo_fig = figure;
hfo_ax = axes(hfo_fig);
hold(hfo_ax, 'on');

hfo_V_eq = load('EquilibriumBranch.mat');
hfo_eq = hfo_V_eq.x(5,:);
V_eq = scale*hfo_V_eq.x(1,:);

plot(hfo_ax, hfo_eq, V_eq, 'Linewidth', lw, 'Color', 'black');

gpi_lc = load('LimitCycleBranch.mat');
max_lc = scale*max(gpi_lc.x(1:4:end-2,:), [], 1);
min_lc = scale*min(gpi_lc.x(1:4:end-2,:), [], 1);
hfo_lc = gpi_lc.x(end,:);

plot(hfo_ax, hfo_lc, min_lc, 'Linewidth', 2, 'Color', 'blue');
plot(hfo_ax, hfo_lc, max_lc, 'Linewidth', 2, 'Color', 'blue');

% Add Hopf marker
for i = 1:length(hfo_V_eq.s)
  if strcmp(hfo_V_eq.s(i).label, 'H ')
    break;
  end
end

plot(hfo_ax, hfo_eq(hfo_V_eq.s(i).index), V_eq(hfo_V_eq.s(i).index), 'Marker', '.', 'Markersize', ms);

xlabel(hfo_ax, 'h_{fo}');
ylabel(hfo_ax, 'V');
set(hfo_ax, 'Fontsize', 32);
set(hfo_ax, 'XLim', [0,1], 'YLim', [-80,50]);

%% Two parameter analysis
gpi_hfo_fig = figure;
gpi_hfo_ax = axes(gpi_hfo_fig);
hold(gpi_hfo_ax, 'on');

for_term = load('SpikeTerminationFLCForward.mat');
gpi_for_term = for_term.x(end-1,:);
hfo_for_term = for_term.x(end,:);

plot(gpi_hfo_ax, gpi_for_term, hfo_for_term, 'Linewidth', lw, 'Color', 'black');

back_term = load('SpikeTerminationFLCBackward.mat');
gpi_back_term = back_term.x(end-1,:);
hfo_back_term = back_term.x(end,:);

plot(gpi_hfo_ax, gpi_back_term, hfo_back_term, 'Linewidth', lw, 'Color', 'black');

xlabel(gpi_hfo_ax, 'g_{NaP}');
ylabel(gpi_hfo_ax, 'h_{fo}');
set(gpi_hfo_ax, 'Fontsize', 32);
set(gpi_hfo_ax, 'XLim', [0,1], 'YLim', [0,1]);

% Return to spiking in 3 variable system
mso_fig = figure;
mso_ax = axes(mso_fig);
hold(mso_ax, 'on');

for_eq = load('EqulibriumBranch.mat');
mso_eq = for_eq.x(4,:);
V_eq = scale*for_eq.x(1,:);

plot(mso_ax, mso_eq, V_eq, 'Linewidth', lw, 'Color', 'black');

for_lc = load('LimitCycles.mat');
max_lc = scale*max(for_lc.x(1:3:end-2,:), [], 1);
min_lc = scale*min(for_lc.x(1:3:end-2,:), [], 1);
mso_lc = for_lc.x(end,:);

plot(mso_ax, mso_lc, min_lc, 'Linewidth', 2, 'Color', 'blue');
plot(mso_ax, mso_lc, max_lc, 'Linewidth', 2, 'Color', 'blue');

for_start = load('SpikeInitationHopfForward.mat');
gpi_for_start = for_start.x(4,:);
mso_for_start = for_start.x(5,:);

mso_gpi_fig = figure;
mso_gpi_ax = axes(mso_gpi_fig);
hold(mso_gpi_ax, 'on');

plot(mso_gpi_ax, gpi_for_start, mso_for_start, 'Linewidth', lw, 'Color', 'black');

back_start = load('SpikeInitiationBackward.mat');
gpi_back_start = back_start.x(4,:);
mso_back_start = back_start.x(5,:);

plot(mso_gpi_ax, gpi_back_start, mso_back_start, 'Linewidth', lw, 'Color', 'black');

xlabel(mso_gpi_ax, 'g_{NaP}');
ylabel(mso_gpi_ax, 'm_{so}');
set(mso_gpi_ax, 'Fontsize', 32);
set(mso_gpi_ax, 'XLim', [0,1], 'YLim', [0,1]);

% Add Hopf marker
for i = 1:length(for_eq.s)
  if strcmp(for_eq.s(i).label, 'H ')
    break;
  end
end

plot(mso_ax, mso_eq(for_eq.s(i).index), V_eq(for_eq.s(i).index), 'Marker', '.', 'Markersize', ms);

xlabel(mso_ax, 'm_{so}');
ylabel(mso_ax, 'V');
set(mso_ax, 'Fontsize', 32);
set(mso_ax, 'XLim', [0,1], 'Ylim', [-80, 50]);

% Folds in (hfo, mso) space - these guarantee that we start in a spiking
% solution
hfo_mso_fig = figure;
hfo_mso_ax = axes(hfo_mso_fig);
hold(hfo_mso_ax, 'on');

hfo_mso_for_lpo = load('FoldLChfoForward.mat');
hfo_mso_for = hfo_mso_for_lpo.x(end,:);
mso_hfo_for = hfo_mso_for_lpo.x(end-1,:);

plot(hfo_mso_ax, mso_hfo_for, hfo_mso_for, 'Linewidth', lw, 'Color', 'black');

hfo_mso_back_lpo = load('FoldLChfoBackward.mat');
hfo_mso_back = hfo_mso_back_lpo.x(end,:);
mso_hfo_back = hfo_mso_back_lpo.x(end-1,:);

plot(hfo_mso_ax, mso_hfo_back, hfo_mso_back, 'Linewidth', lw, 'Color', 'black');

xlabel(hfo_mso_ax, 'm_{so}');
ylabel(hfo_mso_ax, 'h_{fo}');
set(hfo_mso_ax, 'Fontsize', 32);
set(hfo_mso_ax, 'XLim', [0,0.505], 'Ylim', [0,1]);

% Folds in (gpi, mso) space - these guarantee that we start in a spiking
% solution
gpi_mso_fig = figure;
gpi_mso_ax = axes(gpi_mso_fig);
hold(gpi_mso_ax, 'on');

gpi_mso_for_lpo = load('FoldLCgpiForward.mat');
gpi_mso_for = gpi_mso_for_lpo.x(end,:);
mso_gpi_for = gpi_mso_for_lpo.x(end-1,:);

plot(gpi_mso_ax, mso_gpi_for, gpi_mso_for, 'Linewidth', lw, 'Color', 'black');

gpi_mso_back_lpo = load('FoldLCgpiBackward.mat');
gpi_mso_back = gpi_mso_back_lpo.x(end,:);
mso_gpi_back = gpi_mso_back_lpo.x(end-1,:);

plot(gpi_mso_ax, mso_gpi_back, gpi_mso_back, 'Linewidth', lw, 'Color', 'black');

xlabel(gpi_mso_ax, 'g_{NaP}');
ylabel(gpi_mso_ax, 'm_{so}');
set(gpi_mso_ax, 'Fontsize', 32);
set(gpi_mso_ax, 'XLim', [0,1], 'Ylim', [0,0.55]);

%% Now plot slow trajectories
slow_fig = figure;
slow_ax = axes(slow_fig);
hold(slow_ax, 'on');

slow_1 = load('SlowTrajectory1.mat');
slow_2 = load('SlowTrajectory2.mat');
% plot(slow_ax, slow_1.t, slow_1.y, 'Linewidth', 4, 'Color', 'black');

plot(slow_ax, slow_2.t, slow_2.y(:,1), 'Linewidth', 4, 'Color', 'black');
xlabel(slow_ax, 'g_{NaP}');
ylabel(slow_ax, 'm_{so}');
set(slow_ax, 'XLim', [0,1000], 'Ylim', [0,1]);

yyaxis right;
plot(slow_ax, slow_2.t, slow_2.y(:,2), 'Linewidth', 4, 'Color', 'red');
ylabel(slow_ax, 'h_{fo}');
set(slow_ax, 'YColor', 'red');

set(slow_ax, 'Fontsize', 32);
set(slow_ax, 'XLim', [0,1000], 'Ylim', [0,1]);
