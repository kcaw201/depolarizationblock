% Plot two parameter bifurcation diagram showing Hopf points and fold in
% (I_app, g_NaP) space

dataL = load( 'twoParHBLeft.dat');
dataR = load( 'twoParHBRight.dat');
dataLP = load( 'twoParLP.dat');

h = plot( dataL(:,1), dataL(:,2), ...
          dataR(:,1), dataR(:,2), ...
          dataLP(:,1), dataLP(:,2));
        
set( h, 'Linewidth', 8.0);

xlabel( 'Current density (mA/cm^2)');
ylabel( 'g_{NaP} (mho/cm^2)');

grid on;

set( gca, 'Fontsize', 20.0, 'XLim', [0,0.3], 'YLim', [0,0.4],...
      'XTick', 0.0:0.1:0.3, 'YTick', 0.0:0.1:0.4);

legend( {'Left Hopf', 'Right Hopf', 'Fold'});

saveas( 1, 'twoParBifDiag.eps', 'epsc');