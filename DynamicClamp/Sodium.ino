// A fast, persistent Na+ conductance using the Hodgkin-Huxley formalism.
// For speed, the parameters m_inf are pre-computed
// and put in lookup tables stored as global variables.
// Time constant is also fixed

// Declare the lookup table variables
float m_inf[1501] = {0.0};                          // Pre-calculate activation and inactivation parameters

// Generate the lookup tables
void GenerateSodiumLUT() {
  float v;
  for (int x=0; x<1501; x++) {
    v = (float)x/10 - 100.0;                        // We use membrane potentials between -100 mV and +50 mV
    m_inf[x] = 1.0/( 1.0+exp( -(v+50.0)/6.0));
  }
}

// At every time step, calculate the sodium current in the Hodgkin-Huxley manner
float Sodium(float v) {
  static float mNaVar = 0.0;    // activation gate
  float v10 = v*10.0;
  int vIdx = (int)v10 + 1000;
  vIdx = constrain(vIdx,0,1500);
  mNaVar = mNaVar + dt * ( m_inf[vIdx]-mNaVar)/tau_m;
  if (mNaVar < 0.0) mNaVar = 0.0;
  if (mNaVar > 1.0) mNaVar = 1.0;
  float na_current = -gNaP * mNaVar * (v - 50);  // ENa = +50 mV
  return na_current;
}

