// This Processing sketch opens a GUI in which users can specify the
// conductance parameters used by the Teensy microcontroller. There are eight of 
// them at present: shunt conductance (g_shunt, nS), maximum HCN conductance (g_hcn, nS),
// maximum sodium conductance (g_Na, nS), mean excitatory Ornstein-Uhlenbeck
// conductance (m_OU_exc, nS), diffusion constant of excitatory Ornstein-Uhlenbeck
// conductance (D_OU_exc, nS^2/ms), mean inhibitory Ornstein-Uhlenbeck conductance
// (m_OU_inh, nS), diffusion constant of inhibitory Ornstein-Uhlenbeck conductance
// (D_OU_inh, nS^2/ms), and maximum EPSC conductance (g_epsc, nS).
//
// The numbers can be adjusted using the sliders.
//
// Pressing "upload" will send the numbers in the GUI to the microcontroller.
// Pressing "zero" will set all the numbers to zero and send zeros to the microcontroller.
//
// The sketch requires the ControlP5 library.
//
// Last updated 05/20/2018, 7:00 pm - ND, CR


// import libraries
import controlP5.*;
import processing.serial.*;
import java.text.*;
import java.io.*;
import java.nio.*;

// define variables for the ControlP5 object (the GUI) and
// a serial object (the port to communicate with the microcontroller)
ControlP5 dcControl;
Serial myPort;
String[] echo = {};

// initialize the variables set by the GUI
float g_shunt = 0;
float g_hcn = 0;
float g_Na = 0;
float m_OU_exc = 0;
Textfield g_epsc_textfield;


void setup() {

    // specify GUI window size, color, and text case
    size(450,400);
    background(150);
    Label.setUpperCaseDefault(false);

    // create the ControlP5 object, add sliders, specify the font, and add buttons
    dcControl = new ControlP5(this);
    dcControl.addSlider("g_leak", 0, 10, 0, 100, 50, 200, 30);
    dcControl.addSlider("v_leak", -100, -20, 0, 100, 100, 200, 30);
    dcControl.addSlider("g_NaP", 0, 100, 0, 100, 150, 200, 30);
    dcControl.addSlider("tau_m", 0, 20, 0, 100, 200, 200, 30);
    PFont pfont = createFont("Arial", 8, true);
    ControlFont font = new ControlFont(pfont, 18);
    dcControl.setFont(font);
    dcControl.addBang("upload").setPosition(125,300).setSize(60,50).setColorForeground(color(100,100,100));
    dcControl.addBang("zero").setPosition(250,300).setSize(60,50).setColorForeground(color(100,100,100));

    // create the serial port used to communicate with the microcontroller
    myPort = new Serial(this,"COM13",115200);
    myPort.clear();
}



void draw(){
  // nothing to see here: the Processing language requires every sketch to contain a draw() function
}


// Upload the numbers in the GUI to the microcontroller.
void upload(){
      print("Updating ...");
      writetoteensy(g_leak);
      writetoteensy(v_leak);
      writetoteensy(g_NaP);
      writetoteensy(tau_m);
      println(" response from Teensy:");
      if (confirmed()) {         // conductance values shown in GUI and interpreted by Teensy are identical
        dcControl.getController("upload").setColorForeground(color(0,255,0));
      } else {                   // conductance values differ: rounding artifacts or transmission errors
        dcControl.getController("upload").setColorForeground(color(255,0,0));
      }
}



// Zero all the numbers in the GUI and transmit zeros to the microcontroller.
void zero(){
    dcControl.getController("g_shunt").setValue(0.0);
    dcControl.getController("v_leak").setValue(-70.0);
    dcControl.getController("g_NaP").setValue(0.0);
    dcControl.getController("tau_m").setValue(1.0);
    upload();
}


// Compares all numbers from the GUI with the echo from the microcontroller and
// highlights individual values depending on the success of the transmission.
// In addition, the Upload button is colored depending on the update success.
boolean confirmed(){
  boolean valid = true;
  delay(500);                // estimated maximum delay for USB buffer transmission from Teensy to GUI
  readfromteensy();          // receive values at once, but convert individually to avoid problems with GUI delay
  if (float(echo[0]) != g_leak) {
    dcControl.getController("g_leak").setValue(float(echo[0]));
    dcControl.getController("g_leak").setColorValueLabel(color(255,0,0));
    valid = false;
  } else {
    dcControl.getController("g_leak").setColorValueLabel(color(0,255,0));
  }
  if (float(echo[1]) != v_leak) {
    dcControl.getController("v_leak").setValue(float(echo[1]));
    dcControl.getController("v_leak").setColorValueLabel(color(255,0,0));
    valid = false;
  } else {
    dcControl.getController("v_leak").setColorValueLabel(color(0,255,0));
  }
  if (float(echo[2]) != g_NaP) {
    dcControl.getController("g_NaP").setValue(float(echo[2]));
    dcControl.getController("g_NaP").setColorValueLabel(color(255,0,0));
    valid = false;
  } else {
    dcControl.getController("g_NaP").setColorValueLabel(color(0,255,0));
  }
  if (float(echo[3]) != tau_m) {
    dcControl.getController("tau_m").setValue(float(echo[3]));
    dcControl.getController("tau_m").setColorValueLabel(color(255,0,0));
    valid = false;
  } else {
    dcControl.getController("tau_m").setColorValueLabel(color(0,255,0));
  }
  }
  return valid;
}


// The numbers sent to the Teensy as unsigned bytes are echoed
// by the device as strings followed by a newline character
void readfromteensy() {
  while (myPort.available() > 0) {
    String inBuffer = myPort.readString();
    if (inBuffer != null) {
      echo = split(inBuffer, "\n");
      for (String myString : echo) {  // iterate through values
        print(myString + " ");
      };
      println();
    }
  }
}


// The numbers from the GUI (floats) are converted to unsigned bytes
// and written to the Teensy.
void writetoteensy(float foo) {
    byte[] b;
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    b = byteBuffer.putFloat(foo).array();
    for (int y=0; y<4; y++) {    // This is probably not necessary
      int boo = (b[y]&0xFF);     // since negative bytes get converted
      myPort.write(boo);         // when written
    }
}


// dispose() is invoked when the applet window closes.
// It just cleans everything up.
void dispose() {
    print("Stopping ...");
    myPort.clear();
    myPort.stop();
    println(" done.");
}
