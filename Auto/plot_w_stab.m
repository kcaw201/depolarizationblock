%% Depolarization block
% Extract and plot the bifurcation diagram and stability from AUTO solns
%
% J Creaser 29 May 2019

clear

% each branch of the bif saved separately.
dfiles=dir('d.branches*'); 
bfiles=dir('b.branches*'); 

dfilenames={dfiles.name}; 
bfilenames={bfiles.name}; 
nof=numel(dfilenames); 

% Set up plotting
figure; hold on;
% colours
Cols = lines(5);
% dummy points for legend
for cc=1:5
    plot(0,1,'.','color',Cols(cc,:),'markersize',10);
end

%% for each file

for kk=1:nof %for each file.
    clear text iapp v ST
    dfid = fopen(dfilenames{kk},'r'); % open the file

    text = textscan(dfid,'%s','Delimiter','');
    text = text{1};
    dfid  = fclose(dfid);
    % Find Stability
    ST = regexp(text,'\w*Stable:...(\d)','tokens');     % stability number
    ST2 = [ST{:}]; ST3 = str2double([ST2{:}]).';           % creates list

    % There are multiple stability values around special points eg HB
    Sidx = find(~cellfun('isempty',strfind(text,'Stable')));
    PT = text(Sidx); 
    PT2=cellfun(@(x) textscan(x,'%*d %d','MultipleDelimsAsOne',1),PT,'UniformOutput',false);
    PT3 = cell2mat([PT2{:}]'); 
    DPT = find(diff(PT3)>0); DPT = [DPT;length(PT3+1)];
    ST4 = ST3(DPT);
    
    % Extract bifurcation values (for checking)
    idx = find(~cellfun('isempty',strfind(text,'BR    PT  TY  LAB'))) + 1;
    BF = text(idx);
    BF2=cellfun(@(x) x(1:3),BF,'UniformOutput',false);
    % then want the first and third long number in each cell 
    % or from b. file which we do below

    % read in b. file data
    bfid = fopen(bfilenames{kk},'r'); % open the file    
    if kk==1 
        HL = 17; 
    else
        HL=20; 
    end
    %                    i_app    v
    cols='%*d %*d %*d %*d %f %*f %f %*f %*f %*f %*f %*f';
    % scan data
    data = textscan(bfid,cols,'MultipleDelimsAsOne',1,'Headerlines',HL);
    iapp=data{1};
    v=data{2};

    if (length(iapp)-1)-length(ST4)>0
        fprintf(['\n wrong lengths' num2str(length(iapp)-1) 'vs'  num2str(length(ST4))  '\n'])
    end
    
    plot(iapp,v,'-k');
    xlabel('i_app'); ylabel('v')
    for nn=2:length(ST4)-1
        plot(iapp(nn),v(nn),'.','color',Cols(ST4(nn-1),:),'markersize',10);

    end
end

%% 
box on
legend('1','2','3','4','5')
axis([-0.05 0.2 -0.8 0])
