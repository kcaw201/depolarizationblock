# Compute bifurcation diagram for spiking interneuron model
# Jen Creaser 2/5/19

import itertools
import math

#=========================================================================
# Set the constants that define the computations.
#=========================================================================

# equilibria NB not what is in the ODE file h_fi is different to get eq
v=-0.76831
h_fo=0.71309
h_fi=0.72629
m_fo=0.001964
m_so=0.0091653

scale=100.0

i_app=0.0
g_pi=0.2

#--------------------------------------------------------------------------
# Set the variable names and the parameter names:
load(
     unames = {1: 'v', 2: 'h_fo', 3: 'h_fi', 4: 'm_fo', 5:'m_so'}, 
     parnames = { 1: 'i_app',  2: 'g_pi'} ,
    )
    
    
#=========================================================================
#=========================================================================
# Functions
#=========================================================================
#=========================================================================
def initalise(v,h_fo,h_fi,m_fo,m_so,i_app,g_pi): 

    # Initialize parameters:
    p = {}
    p[1] = i_app
    p[2] = g_pi
    
    print p,v,h_fo,h_fi,m_fo,m_so
    
    s=load([v,h_fo,h_fi,m_fo,m_so],PAR=p)
    save(s,'start')
    
#=========================================================================
def continuation():

    print "\n***Compute stationary solutions***"
    print   ' ------------------------------------------'
    
    dpb = rn(e='dpb',c='dpb',s='start',sv='branches0')
    
    print "\n***Compute the First periodic solution family***"
    run(dpb('HB1'),IPS=2,ICP=['i_app','PERIOD'],NPR=10,NMX=70,DS=0.001,UZR={-1:[0.5]},sv='branches1')
    pl('branches1')
    
    print "\n***Compute the Second periodic solution family***"
    run(dpb('HB2'),IPS=2,ICP=['i_app','PERIOD'],NPR=10,NMX=80,DS=0.001, UZR={-1:[0.5]},sv='branches2')
    pl('branches2')
    
    print "\n***Compute the Third periodic solution family***"
    run(dpb('HB3'),IPS=2,ICP=['i_app','PERIOD'],NPR=10,NMX=80,DS=0.001, UZR={-1:[0.5]},sv='branches3')
    pl('branches3')
    wait()
    
 
#=========================================================================
# A utility function that removes symmetric solutions
def remove_symmetric(unreduced, reduced):
    
    def check_symmetric(s1,s2):
        end=len(s1["r1"])
        return (abs(abs(s1["r1"][end-1])-abs(s2["r1"][end-1]))<1e-3
                and abs(abs(s1["r2"][end-1])-abs(s2["r2"][end-1]))<1e-3
                and abs(abs(s1["r3"][end-1])-abs(s2["r3"][end-1]))<1e-3
                and abs(abs(s1["r4"][end-1])-abs(s2["r4"][end-1]))<1e-3)
    dlb(check_symmetric,unreduced,reduced)
    print 'Reduced data saved in s.',reduced
    rl(reduced)
    cl()
#=========================================================================
#=========================================================================

#=========================================================================
#=========================================================================
# MAIN PROGRAM
#=========================================================================
#=========================================================================

#---------------------------
# Generate the starting data
#---------------------------
initalise(v,h_fo,h_fi,m_fo,m_so,i_app,g_pi)
    
#---------------------------
# Continue in beta
#---------------------------
continuation()

#---------------------------
# remove symmetric branches
#---------------------------
#remove_symmetric('branches',bifcurves)
#print 'symmetry removed, data saved in s.',bifcurves
#print '\n--------------------------------------------------'

cl()

