!----------------------------------------------------------------------
!----------------------------------------------------------------------
!   dpb :    Depolarization block 
!               bifurcation diagram
!----------------------------------------------------------------------
!----------------------------------------------------------------------

      SUBROUTINE FUNC(NDIM,U,ICP,PAR,IJAC,F,DFDU,DFDP)
!     ---------- ----

      IMPLICIT NONE
      INTEGER NDIM, IJAC, ICP(*)
      DOUBLE PRECISION U(NDIM), PAR(*), F(NDIM), DFDU(*), DFDP(*)
      DOUBLE PRECISION v,h_fo,h_fi,m_fo,m_so,i_app, g_pi
      DOUBLE PRECISION m_fi_inf,h_fi_inf,m_pi_inf,m_fo_inf,h_fo_inf,m_so_inf 
      DOUBLE PRECISION i_fi, i_pi, i_fo, i_so, i_L
      
        ! variables
        v = U(1) 
        h_fo = U(2) 
        h_fi = U(3)
        m_fo = U(4)
        m_so = U(5)
       
        ! Parameters
        i_app=PAR(1)
        g_pi = Par(2)

        ! Fast inward current
        m_fi_inf = 1/(1+exp(-(v+0.37)/0.05))
        h_fi_inf = 1/(1+exp(-(v+0.70)/(-0.07)))
        ! Slow inward current
        m_pi_inf = 1/(1+exp(-(v+0.47)/0.03))
        ! Fast outward current
        m_fo_inf = 1/(1+exp(-(v+0.058)/0.114))
        h_fo_inf = 1/(1+exp(-(v+0.68)/(-0.097)))
        ! Slow outward current
        m_so_inf = 1/(1+exp(-(v+0.30)/0.10))
        ! Ion currents
        ! Fast inward current
        i_fi = 130.0*m_fi_inf**3*h_fi*(v-0.60)
        ! Slow inward current
        i_pi = g_pi*m_pi_inf*(v-0.60)
        ! Fast outward current
        i_fo = 10.0*m_fo*h_fo*(v+0.85)
        ! Slow outward current
        i_so = 1.65*m_so*(v+0.85)
        ! Leak current
        i_L = 0.02*(v+0.65)
  
        F(1) = - i_fi - i_pi - i_fo - i_so - i_L + i_app        ! dv/dt 
        F(2) = (h_fo_inf - h_fo)/1400.0                         ! dh_fo/dt
        F(3) = (h_fi_inf - h_fi)                                       ! dh_fi/dt 
        F(4) = (m_fo_inf - m_fo)                                  ! dm_fo/dt
        F(5) = (m_so_inf - m_so)/75.0                        ! dm_so/dt

      END SUBROUTINE FUNC
!----------------------------------------------------------------------
!----------------------------------------------------------------------
      SUBROUTINE STPNT
      END SUBROUTINE STPNT

      SUBROUTINE BCND 
      END SUBROUTINE BCND

      SUBROUTINE ICND 
      END SUBROUTINE ICND

      SUBROUTINE FOPT 
      END SUBROUTINE FOPT

      SUBROUTINE PVLS
      END SUBROUTINE PVLS
!----------------------------------------------------------------------
!----------------------------------------------------------------------
