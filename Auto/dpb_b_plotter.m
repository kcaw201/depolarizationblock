%% This script, breader, reads the b. files and pulls out the UZR points.
clear; close all; clc;

files=dir('b.bif_i_app_g_pi*'); % find all the b.files

filenames={files.name}; % lists the filenames in an array
nof=numel(filenames); % returns the number of elements, nof, in array filenames - ie number of files

fig = figure( 'Position', [400 400 1200 800]);
ax  = axes( fig);

hold( ax);
xlabel( ax, 'Current injection (mA/cm^2)');
ylabel( ax, 'g_{NaP}');
box on;
colr=lines(nof);

labels = cell( nof, 1);
h = gobjects( nof, 1);

for i=3:6 %for each file.
    fnam = filenames{i};
    if contains( fnam, 'homo')
%       continue;
    end

    fid = fopen(fnam,'r'); % open the file
    lnam = fnam(18:end);
    labels{i} = lnam;
    % load start values for variables
    n=0; m=0;
    type = 0;
    
    while 1 % b file may have several parts to it.
        HL = 21;    % open b. file and check how many headerlines there are
        % this is the structure of the columns of the file, always 4
        % integers followed by the parameters/variables.
        cols='%*d %*d %d %d %20.18f %*20.18f %*20.18f %*20.18f %*20.18f %*20.18f %*20.18f %20.18f';
        % scan data
        data = textscan(fid,cols,'MultipleDelimsAsOne',1,'Headerlines',HL);
        i_app=data{3}; 
        g_pi=data{4};
        TY = data{1}; LAB = data{2};
        % take off the last entry as text scan reads one too far
        i_app=i_app(1:end-1,1);
        g_pi=g_pi(1:end-1,1);
        
        bif_ind = find( LAB~=0);
        bif_ind(end) = [];
        bif_I = i_app(bif_ind);
        bif_g = g_pi(bif_ind);
        
        h(i) = plot( ax, i_app, g_pi,'-', 'color', colr(i,:), 'linewidth', 2, 'Displayname', lnam);
%         plot( ax, bif_I, bif_g, 'Marker', '.', 'Markersize', 50, 'Linestyle', 'none');
        
        if feof(fid) > 0
            break
        end
    end

    fclose(fid);

end    

% For main figure
set( ax, 'Fontname', 'Arial', 'Fontsize', 20);
set( ax, 'XLim', [-0.1, 0.3], 'YLim', [0,0.4]);
set( ax, 'XTick', -0.1:0.1:0.3, 'YTick', 0:0.1:0.4);
legend( ax, h, labels);

% For inset only
% set( ax, 'XLim', [0.035 0.06], 'YLim', [0.085,0.115]);
% set( ax, 'XTick', [], 'YTick', []);
% xlabel( ax, '');
% ylabel( ax, '');