# Compute bifurcation diagram for spiking interneuron model
# Jen Creaser 2/5/19

import itertools
import math

#=========================================================================
# Set the constants that define the computations.
#=========================================================================

# equilibria NB not what is in the ODE file h_fi is different to get eq
v=-0.76831
h_fo=0.71309
h_fi=0.72629
m_fo=0.001964
m_so=0.0091653

scale=100.0

i_app=0.0
g_pi=0.2

#--------------------------------------------------------------------------
# Set the variable names and the parameter names:
load(
     unames = {1: 'v', 2: 'h_fo', 3: 'h_fi', 4: 'm_fo', 5:'m_so'}, 
     parnames = { 1: 'i_app',  2: 'g_pi'} ,
    )
    
    
#=========================================================================
#=========================================================================
# Functions
#=========================================================================
#=========================================================================
def initalise(v,h_fo,h_fi,m_fo,m_so,i_app,g_pi): 

    # Initialize parameters:
    p = {}
    p[1] = i_app
    p[2] = g_pi
    
    print p,v,h_fo,h_fi,m_fo,m_so
    
    s=load([v,h_fo,h_fi,m_fo,m_so],PAR=p)
    save(s,'start')
    
#=========================================================================
def continuation_i_app():

    print "\n***Continue in i_app***" # detect hopf bifs
    print   ' ------------------------------------------'
    r1=rn(e='dpb',c='dpb',s='start')
    save(r1,'bif_i_app_gpi0-2')
    pl('bif_i_app_gpi0-2')
    wait()
    
    
#=========================================================================
def playing():

    print "\n***Continue in i_app***" # detect hopf bifs
    print   ' ------------------------------------------'
    r1=rn(e='dpb',c='dpb',s='start')
    save(r1,'1')
    pl('1')
    #wait()
    
    print "\n\n***Continue HB1 periodic orbit to high period***\n"
    r2=rn(r1('HB4'),IPS=2,NPR=100,ICP=['i_app','PERIOD'],NTST=35,UZSTOP={'i_app':0.09,11:6e3})
    ap(r2,'1')
    pl('1')
    rl('1')
    #wait()
    
    
    r3=rn(r2('UZ1'),IPS=2,NPR=100,ICP=['g_pi','PERIOD'],NTST=35,UZSTOP={'g_pi':[0.0,0.4]})
    save(r3,'3')
    rl('3')
    pl('3')
    wait()
    
    
    
    
    
    
    
#=========================================================================
def continuation_g_pi():

    print "\n***Continue in g_pi down to g_pi = 0***" 
    print   ' ------------------------------------------'
    rn(e='dpb', c='dpb', s='start', ICP=['g_pi'], DS='-',UZSTOP={'g_pi': [0.0]}, sv='g_pi_start')
    #rn(e='dpb', c='dpb', s='start', ICP=['g_pi'],UZSTOP={'g_pi': [0.0]}, ap='g_pi_start')
    plot('g_pi_start')
    #wait()
    
    # then switch to compute bif diag in i_app for new g_pi value
    r1=rn(e='dpb',c='dpb',s='g_pi_start',IRS='UZ1',UZSTOP = {'i_app':[-0.2,0.2]},NMX=2000,sv='bif_i_app_gpi0')

    # compute the po's coming from the HBs
    print "\n\n***Continue HB1 periodic orbit to high period***"
    r2=rn(r1('HB1'),IPS=2,NPR=100000000,NMX = 20000,NTST=100,ICP=['i_app','PERIOD'],UZSTOP={11:5e3})
    ap(r2,'bif_i_app_gpi0')
    
    pl('bif_i_app_gpi0')
    wait()

#=========================================================================
def lppts():      # Continue the two fold points

    r1=rn(e='dpb',c='dpb',s='start')
        
    print "\n\n***Continue LP1 point***"
    r20=rn(r1('LP1'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r20,'bif_i_app_g_pi_fold1')
    r21=rn(r1('LP1'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r21,'bif_i_app_g_pi_fold1')
    
    rl('bif_i_app_g_pi_fold1')    # relabel
    pl('bif_i_app_g_pi_fold1')
    wait()
    
    print "\n\n***Continue LP2 point***"
    r20=rn(r1('LP2'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r20,'bif_i_app_g_pi_fold2')
    r21=rn(r1('LP2'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r21,'bif_i_app_g_pi_fold2')

    rl('bif_i_app_g_pi_fold2')    # relabel
    pl('bif_i_app_g_pi_fold2')
    wait()
    
#=========================================================================
def hb1():      # continue from hopf 1

    r1=rn(e='dpb',c='dpb',s='start')
        
    print "\n\n***Continue HB1 point***\n"
    r20=rn(r1('HB1'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r20,'bif_i_app_g_pi_HB1')
    r21=rn(r1('HB1'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r21,'bif_i_app_g_pi_HB1')
    
    print "\n\n***Continue HB1 periodic orbit to high period***\n"
    r2=rn(r1('HB1'),IPS=2,NPR=100,ICP=['i_app','PERIOD'],NTST=35,UZSTOP={11:6e3})
    save(r2,'bif_i_app_HB1_po')

    print "\n\n***Set up homcont = phase shift the orbit, auto detect equilibrium***\n"  
    r3=rn(r2,IPS=9,ICP=['i_app','g_pi'],NPR=3,NMX=2,ISTART=4)
   
    print "\n\n***Continue HB1 Homoclinic orbit + ***\n"
    r4=rn(r3,IPS=9,ICP=['i_app','g_pi'],NPR=100,NMX=1000,NTST=50,ISTART=1,UZSTOP={'g_pi':[1.25613E-01]})
    ap(r4,'bif_i_app_g_pi_HB1-homo')
  
    print "\n\n***Continue Homoclinic orbit - ***\n"
    r5=rn(r3,IPS=9,ICP=['i_app','g_pi'],DS='-',NPR=1000,NMX=3000,NTST=50,ISTART=1,UZSTOP={'g_pi':[-0.5,0.5]})
    ap(r5,'bif_i_app_g_pi_HB1-homo')
    rl('bif_i_app_g_pi_HB1-homo')
    pl('bif_i_app_g_pi_HB1-homo')
    wait()
    
#=========================================================================
def hb2():      # continue from hopf 2

    r1=rn(e='dpb',c='dpb',s='start')    
    
    print "\n\n***Continue HB2 point***\n" 
    r22=rn(r1('HB2'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r22,'bif_i_app_g_pi_HB2')
    r23=rn(r1('HB2'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r23,'bif_i_app_g_pi_HB2')
    
    print "\n\n***Continue first periodic orbit to high period***\n"
    r2=rn(r1('HB2'),IPS=2,ICP=['i_app','PERIOD'],NTST=35,UZSTOP={11:3e4})
    save(r2,'bif_i_app_HB2_po')

    print "\n\n***Set up homcont = phase shift the orbit, auto detect equilibrium***\n" #
    r3=rn(r2,IPS=9,ICP=['i_app','g_pi'],NPR=3,NMX=2,ISTART=4)
   
    print "\n\n***Continue Homoclinic orbit + ***\n"
    r4=rn(r3,IPS=9,ICP=['i_app','g_pi'],NPR=100,NMX=1000,NTST=20,ISTART=1,UZSTOP={'i_app':[ 4.68622E-02 ],'g_pi':[0.5]})
    ap(r4,'bif_i_app_g_pi_HB2-homo')
  
    print "\n\n***Continue Homoclinic orbit - ***\n"
    r5=rn(r3,IPS=9,ICP=['i_app','g_pi'],DS='-',NPR=1000,NMX=3000,NTST=20,ISTART=1,UZSTOP={'g_pi':[0.5]})
    ap(r5,'bif_i_app_g_pi_HB2-homo')
    rl('bif_i_app_g_pi_HB2-homo')
    pl('bif_i_app_g_pi_HB2-homo')
    wait()
    
#=========================================================================
def hb3():      # continue from hopf 3

    r1=rn(e='dpb',c='dpb',s='start')
    
    print "\n\n***Continue HB3 point***\n"
    r24=rn(r1('HB3'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r24,'bif_i_app_g_pi_HB3')
    r25=rn(r1('HB3'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r25,'bif_i_app_g_pi_HB3')

    print "\n\n***Continue first periodic orbit to high period***\n"
    r2=rn(r1('HB3'),IPS=2,ICP=['i_app','PERIOD'],NTST=50,UZSTOP={11:5e4})
    save(r2,'bif_i_app_HB3_po')

    print "\n\n***Set up homcont = phase shift the orbit, auto detect equilibrium***\n" #
    r3=rn(r2,IPS=9,ICP=['i_app','g_pi'],NPR=3,NMX=2,NTST=100,ISTART=4)
   
    print "\n\n***Continue Homoclinic orbit + ***\n"
    r4=rn(r3,IPS=9,ICP=['i_app','g_pi'],NPR=100,NMX=3000,NTST=20,ISTART=1,UZSTOP={'i_app':[8.44959E-02]})
    ap(r4,'bif_i_app_g_pi_HB3-homo')
  
    print "\n\n***Continue Homoclinic orbit - ***\n"
    r5=rn(r3,IPS=9,ICP=['i_app','g_pi'],DS='-',NPR=1000,NMX=3000,NTST=20,ISTART=1,UZSTOP={'g_pi':[-0.5,0.5]})
    ap(r5,'bif_i_app_g_pi_HB3-homo')
    rl('bif_i_app_g_pi_HB3-homo')
    pl('bif_i_app_g_pi_HB3-homo')
    wait()

#=========================================================================
def hb4():      # continue from hopf 4

    r1=rn(e='dpb',c='dpb',s='start')
    
    print "\n\n***Continue HB4 point***\n"
    r24=rn(r1('HB4'),ISW=2,ICP=['i_app','g_pi'],UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    sv(r24,'bif_i_app_g_pi_HB4')
    r25=rn(r1('HB4'),ISW=2,ICP=['i_app','g_pi'],DS='-',UZSTOP={'i_app':[0.2, -0.2], 'g_pi':[-0.5,0.5]})
    ap(r25,'bif_i_app_g_pi_HB4')
    
    print "\n\n***Continue first periodic orbit to high period***\n"
    r2=rn(r1('HB4'),IPS=2,ICP=['i_app','PERIOD'],UZSTOP={11:5e4})
    save(r2,'bif_i_app_HB4_po')

    print "\n\n***Set up homcont = phase shift the orbit, auto detect equilibrium***\n" #
    r3=rn(r2,IPS=9,ICP=['i_app','g_pi'],NPR=3,NMX=2,NTST=100,ISTART=4)
   
    print "\n\n***Continue Homoclinic orbit + ***\n" # MX at 9.01582E-02
    r4=rn(r3,IPS=9,ICP=['i_app','g_pi'],NPR=200,NMX=200,ISTART=1,UZSTOP={'i_app':5.58793E-02})
    ap(r4,'bif_i_app_g_pi_HB4-homo')
  
    print "\n\n***Continue Homoclinic orbit - ***\n"
    r5=rn(r3,IPS=9,ICP=['i_app','g_pi'],DS='-',NPR=1000,NMX=3000,ISTART=1,UZSTOP={'g_pi':[-0.5,0.5]})
    ap(r5,'bif_i_app_g_pi_HB4-homo')
    rl('bif_i_app_g_pi_HB4-homo')
    pl('bif_i_app_g_pi_HB4-homo')
    wait()


#=========================================================================
#=========================================================================
# MAIN PROGRAM
#=========================================================================
#=========================================================================

#playing()
#---------------------------
# Generate the starting data
#---------------------------
#initalise(v,h_fo,h_fi,m_fo,m_so,i_app,g_pi)
    
#---------------------------
# Continue to detect hopf points
#---------------------------
#continuation_i_app()

# for funky snaking diag run this:
continuation_g_pi() 

# Follow the fold points
#lppts()

# Follow the homoclinic orbits
#hb1()
#hb2()
#hb3()
#hb4()



cl()

