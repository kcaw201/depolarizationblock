%% pete's code for plotting a peridoic orbit from the s. file and the vectors that come off it.
%% THIS IS FOR A 3D system and will need adjusting to work for ths system

solfn = 's.poxy_rho28_wu_evec';

% Load solution points and vectors
NTPL = textread([solfn],'%*f %*f %*f %*f %*f %*f %f %*f %*f %*f %*f %*f %*f %*f %*f %*f',1)

%%
[t,Xv,Yv,Zv,Xnv,Ynv,Znv] = textread([solfn],'%f %f %f %f %f %f %f','headerlines', 1);
  %%  
% Only require the solution part of the solution file
t = t(1:NTPL);
Xv = Xv(1:NTPL);
Yv = Yv(1:NTPL);
Zv = Zv(1:NTPL);
Xnv = Xnv(1:NTPL);
Ynv = Ynv(1:NTPL);
Znv = Znv(1:NTPL);

%% Plot the vectors and PO

figure(1);
hold on;

% Normalised vectors
a = -2./sqrt(Xnv.^2 + Ynv.^2 + Znv.^2);

plot3([Xv Xv+a.*Xnv]',[Yv Yv+a.*Ynv]', [Zv Zv+a.*Znv]','r','Linewidth',1) 

view(0,0)   % XZ
axis off

set(gca,'position',[0.01 0.01 0.98 0.98]) 

set(gca,'DataAspectRatio',[1 1 1], 'PlotBoxAspectRatio',[1 1 1])
