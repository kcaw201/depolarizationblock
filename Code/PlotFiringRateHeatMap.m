% Plot firing rates for Nowacki model under variation of I for different
% values of g_NaP
% Kyle Wedgwood
% 17.4.2019

close all; clc; clear;

X0 = [-0.65;0.1;0.1;0.9;0.9];
g_NaP = 0.2;
g_NaT = 130;

hls = feval( @NowackiSimplifiedScaled);

% Start at steady state
X0 = fsolve( @(x) hls{2}( 0, x, g_NaP, g_NaT, 0.0), X0, optimoptions( 'fsolve', 'Display', 'iter'));

% Applied currents
nJ = 1000;
max_curr = 0.4;
Gt = 130:50:280;
Gp = 0.0:0.1:0.4;
nG = length( Gp);

J = linspace( 0.0, max_curr, nJ);

% Stepper options
options = odeset( 'RelTol', 1e-8);
f = zeros( nJ, nG);

stim_time = 500;
labels = cell( 1, 3);

for j = 1:nG
  for i = 2:nJ
    [tStim,yStim] = ode45( hls{2}, [0,stim_time], X0, options, Gp(j), g_NaT, J(i));
    p = findpeaks( yStim(:,1), 'MinPeakHeight', 0.0);
    f(i,j) = numel( p);
    fprintf( 'Done %d of %d of %d\n', i, j, nJ);
  end
  labels{j} = sprintf( 'g_{NaP} = %0.1f', Gp(j));
end

%% Plot figure
fig = figure;
ax  = axes( fig);
plot( ax, J, f, 'Linewidth', 4.0);
xlabel( ax, 'Current density (mA/cm^2)');
ylabel( ax, 'APs/500ms');
set( ax, 'YLim', [0,120]);
set( ax, 'YTick', 0:50:100);
set( ax, 'Fontsize', 20);

legend( labels);
title( sprintf( 'g_{NaT} = %0.1f', g_NaT));
box off;
