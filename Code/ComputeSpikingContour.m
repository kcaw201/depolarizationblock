% Analyse data to find spiking contour
% Kyle Wedgwood
% 3.9.2019

function [spike_flag,contour_mat] = ComputeSpikingContour( max_dv, AP_thresh, g_NaP, I_app)

  spike_flag = zeros( length( g_NaP), length( I_app));
  
  fig = figure( 'Position', [400, 400, 1200, 1200]);
  ax  = axes( fig);
  
  h = imagesc( ax, g_NaP, I_app, spike_flag);
  caxis( ax, [0, 1]);
  xlabel( 'Current density (AU)');
  ylabel( 'g_{NaP} (AU)');
  set( ax, 'YDir', 'normal');
  set( ax, 'XTick', 0:0.1:0.4);
  set( ax, 'YTick', 0:0.1:0.4);
  set( ax, 'Fontsize', 32);
  
  spike_flag = (max_dv > AP_thresh);
  set( h, 'CData', spike_flag);
  
  contour_mat = contour( I_app, g_NaP, spike_flag, [0.5,0.5]);
  
end