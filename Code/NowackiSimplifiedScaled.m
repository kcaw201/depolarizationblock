% Nowacki model with fast transient inward current, fast persisent inward
% current, fast delayed rectifying outward current and slow persistent
% outward current
% Voltage scaled by 100 to keep variability in line with gating variables
% As in Alices Coles 2019 dissertation
% Kyle Wedgwood
% 19.4.2019

function out = NowackiSimplifiedScaled
out{1} = @init;
out{2} = @fun_eval;
out{3} = @jacobian;
out{4} = @jacobianp;
out{5} = [];
out{6} = [];
out{7} = [];
out{8} = [];
out{9} = [];

% --------------------------------------------------------------------------
function dydt = fun_eval(~,kmrgd,p_gpi,p_gti,p_Iapp)
  % Define parameters
  scale = 100.0;

  E_i = 60.0/scale;
  E_o = -85.0/scale;
  E_l = -65.0/scale;

  C = 1.0;

  V_mti = -37.0/scale;
  V_mpi = -47.0/scale;
  k_mti = 5.0/scale;
  k_mpi = 3.0/scale;

  V_x   = [ -5.8; -30.0; -70.0; -68.0]/scale;
  k_x   = [ 11.4; 10.0; -7.0; -9.7]/scale;
  tau_x = [ 1.0; 75.0; 1.0; 1400.0];

  % Original outward current conductances
  g_fo = 10.0;
  g_so = 1.65;
  g_l  = 0.02;

%   % Both fast and slow outward current
%   g_fo = 10.0;
%   g_so = 1.0;
%   g_l  = 0.1;

%   % Only transient outward current
%   g_fo = 10.0;
%   g_so = 0.0;
%   g_l  = 0.1;

% %   % Only persisent outward current - doesn't really produce APs
%   g_fo = 0.0;
%   g_so = 20.0;
%   g_l  = 0.2;

  % Steady state activation for Na+ channels
  mti = 1.0/( 1.0+exp( -( kmrgd(1)-V_mti)/k_mti));
  mpi = 1.0/( 1.0+exp( -( kmrgd(1)-V_mpi)/k_mpi));

  % Other steady state values
  x_inf = 1.0./( 1.0+exp( -( kmrgd(1)-V_x)./k_x));

  % Define currents
  I_ti = p_gti*mti.^3*kmrgd(4)*( kmrgd(1)-E_i);
  I_pi = p_gpi*mpi*( kmrgd(1)-E_i);
  I_fo = g_fo*kmrgd(2)*kmrgd(5)*( kmrgd(1)-E_o);
  I_so = g_so*kmrgd(3)*( kmrgd(1)-E_o);
  I_l  = g_l*( kmrgd(1)-E_l);

  dydt=[ -1.0/C*( I_ti + I_pi + I_fo + I_so +I_l - p_Iapp);
         (x_inf-kmrgd(2:5))./tau_x ];

% --------------------------------------------------------------------------
function [tspan,y0,options] = init()
handles = feval(NowackiSimplifiedScaled);
y0=[-65.0,0,0,0,0];
options = odeset('Jacobian',handles(3),'JacobianP',handles(4));
tspan = [0 100];

% --------------------------------------------------------------------------
function jac = jacobian(~,kmrgd,p_gpi,p_gti,~)
  % Define parameters
  scale = 100.0;

  E_i = 60.0/scale;
  E_o = -85.0/scale;

  C = 1.0;

  V_mti = -37.0/scale;
  V_mpi = -47.0/scale;
  k_mti = 5.0/scale;
  k_mpi = 3.0/scale;

  V_x   = [ -5.8; -30.0; -70.0; -68.0]/scale;
  k_x   = [ 11.4; 10.0; -7.0; -9.7]/scale;
  tau_x = [ 1.0; 75.0; 1.0; 1400.0];

  g_fo = 10.0;
  g_so = 1.65;
  g_l  = 0.02;

  % Steady state values
  mti = 1.0/( 1.0+exp( -( kmrgd(1)-V_mti)/k_mti));
  mpi = 1.0/( 1.0+exp( -( kmrgd(1)-V_mpi)/k_mpi));

  x_inf_p = @(V_x,k_x) exp( -(kmrgd(1)-V_x)/k_x)/( k_x*( 1.0+exp( -(kmrgd(1)-V_x)/k_x)).^2);

  jac = [ -1.0/C*( p_gti*mti.^3*kmrgd(4) ...
     + 3*p_gti*mti^2*x_inf_p( V_mti, k_mti)*kmrgd(4)*( kmrgd(1)-E_i) ...
     + p_gpi*mpi ...
     + p_gpi*x_inf_p( V_mpi, k_mpi)*( kmrgd(1)-E_i) ...
     + g_fo*kmrgd(2)*kmrgd(5) ...
     + g_so*kmrgd(3) + g_l), -g_fo/C*kmrgd(5)*( kmrgd(1)-E_o), -g_so*( kmrgd(1)-E_o), -p_gti*mti^3/C*( kmrgd(1)-E_i), -g_fo*kmrgd(2)/C*( kmrgd(1)-E_o);
  x_inf_p( V_x(1), k_x(1))/tau_x(1), -1.0/tau_x(1), 0, 0, 0;
  x_inf_p( V_x(2), k_x(2))/tau_x(2), 0, -1.0/tau_x(2), 0, 0;
  x_inf_p( V_x(3), k_x(3))/tau_x(3), 0, 0, -1.0/tau_x(3), 0;
  x_inf_p( V_x(4), k_x(4))/tau_x(4), 0, 0, 0, -1.0/tau_x(4)];

% fprintf( 'V = %f\n', kmrgd(1));
if isnan( jac(1,1)) == 1
  pause
end
% --------------------------------------------------------------------------
function jacp = jacobianp(~,kmrgd,~,~,~)
  scale = 100.0;

  E_i = 60.0/scale;
  C = 1.0;
  mti = 1.0/( 1.0+exp( -( kmrgd(1)+37.0/scale)/(5.0/scale)));
  mpi = 1.0/( 1.0+exp( -( kmrgd(1)+47.0/scale)/(3.0/scale)));

  jacp = zeros( 5, 3);
  jacp(1,:) = [ 1.0/C, -1.0/C*mpi*( kmrgd(1)-E_i), -1.0/C*mti.^3*kmrgd(4)*( kmrgd(1)-E_i)];
% --------------------------------------------------------------------------
