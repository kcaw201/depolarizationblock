% Create heat map of firing rates under variation of G_NaP and G_NaT
% Kyle Wedgwood
% 2.9.2019

close all; clc; clear;

if isempty( gcp( 'nocreate'))
  parpool( 12);
end

hls = feval( @NowackiSimplifiedScaled);
fun = hls{2};

% Applied currents
no_current_steps = 500;
max_curr = 0.4;

min_NaP = 0.0;
max_NaP = 0.1;
no_NaP_steps = 500;

g_NaP = linspace( min_NaP, max_NaP, no_NaP_steps);
I_app = linspace( 0.0, max_curr, no_current_steps);

% Stepper options
options = odeset( 'RelTol', 1e-8);
gradient_thresh = 0.008;
AP_thresh = 0.2;
stim_time = 500;

% Output variables
no_APs = zeros( no_NaP_steps, no_current_steps);
end_dv = zeros( size( no_APs));
max_dv = zeros( size( no_APs));

% Save last point to process later
final_point = cell( size( no_APs));

g_NaT = 230.0;
time  = linspace( 0, stim_time, 10000);

parfor g_step = 1:no_NaP_steps
  
  I_app = linspace( 0.0, max_curr, no_current_steps);
  
  for current_step = 1:no_current_steps
    
    X0 = [-0.65;0.1;0.1;0.9;0.9];
    
    % Start at steady state
    X0 = fsolve( @(x) hls{2}( 0, x, g_NaP(g_step), g_NaT, 0.0), X0, ...
      optimoptions( 'fsolve', 'Display', 'off'));

    sol = ode45( hls{2}, [0,stim_time], X0, options, ...
      g_NaP(g_step), g_NaT, I_app(current_step));
    
    [~,loc] = findpeaks( sol.y(1,:)', 'MinPeakHeight', 0.0);
    
    no_APs(g_step,current_step) = numel( loc);
    
    final_point{g_step,current_step}.state = sol.y(:,end);
    final_point{g_step,current_step}.g_NaP = g_NaP(g_step);
    final_point{g_step,current_step}.I_app = I_app(current_step);
    
    % Compute whether cell is spiking or not
    [~,dv] = deval( sol, time, 1);
    max_dv(g_step,current_step) = max( dv);
    
    % Identify depolarisation block via gradient of last point
    if numel( loc) > 1
      dv = feval( hls{2}, 0, sol.y(:,end), g_NaP(g_step), g_NaT, ...
        I_app(current_step));
      end_dv = norm( dv);
    end
    
    fprintf( 'Done (%d, %d) of (%d, %d)\n', current_step, g_step, ...
      no_current_steps, no_NaP_steps);
  end
end

% Save values
save( sprintf( 'heatmap_gnat_%.0f_new_both_K.mat', g_NaT), 'no_APs', 'end_dv', ...
  'max_dv', 'final_point', 'g_NaP', 'I_app', 'gradient_thresh');
