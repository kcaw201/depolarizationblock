% Show trajectories for different parameter values for
% NowackiSimplifiedScaled model to show depolarisation block and shift in
% firing threshold
close all; clc; clear;

X0 = [-0.65;0.1;0.1;0.9;0.9];
g_NaT = 280.0;
g_NaP = 0.4;

hls = feval( @NowackiSimplifiedScaled);

% Start at steady state
X0 = fsolve( @(x) hls{2}( 0, x, g_NaP, g_NaT, 0.0), X0, optimoptions( 'fsolve', 'Display', 'iter'));

% Applied currents
max_curr = 0.3;
J = (1:6)*max_curr/6;

%% Do initial time simulation
options = odeset( 'RelTol', 1e-8);
[tPre,yPre] = ode45( hls{2}, [0,100], X0, options, g_NaP, g_NaT, 0.0);
XInit = yPre(end,:);

% Reorder indices
ind = [1 3 5 2 4 6];

%% Make plot
f = figure;

for i = 1:6  
  ax(i) = subplot( 3, 2, ind(i)); hold on;
  set( ax(i), 'Fontsize', 20, 'YLim', [-100,60]); grid on;
  title( sprintf( 'I_{app} = %.2f', J(i)), 'Fontsize', 20);
  
  if i<=3
    ylabel( 'Voltage (mV)');
  end
  if ~mod(i,3)
    xlabel( 'Time (ms)');
  end
  
  plot( tPre, 100*yPre(:,1), 'Linewidth', 4.0, 'Color', 'blue');
  
  [tStim,yStim] = ode45( hls{2}, tPre(end)+[0,400], XInit, options, g_NaP, g_NaT, J(i));
  X0 = yStim(end,:)';
  
  [tPost,yPost] = ode45( hls{2}, tStim(end)+[0,100], X0, options, g_NaP, g_NaT, 0.0);
  
  h = plot( tPre, 100*yPre(:,1), tStim, 100*yStim(:,1), tPost, 100*yPost(:,1));
  
  for j = 1:length(h)
    set( h(j), 'Color', 'blue', 'Linewidth', 2.0);
  end
  
end

mtit( sprintf( 'g_{NaT} = %0.f, \t g_{NaP} = %0.1f', g_NaT, g_NaP), 'Fontsize', 32);
