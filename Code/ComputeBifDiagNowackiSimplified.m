% Generate bifurcation diagram for NowackiSimplified model with I_app
% as control parameter
% DOES NOT CURRENTLY WORK - GETTING STEP-SIZE TOO SMALL ERROR, EVEN WITH
% TINY STEPSIZE
% Kyle Wedgwood
% 17.4.2019

debug = false;

run( '~/Dropbox/matcont6p11/init');

X0 = [-65.0;0.1;0.1;0.9;0.9];
P0 = [0.2;0.0];

hls = feval( @NowackiSimplified);

% Start at steady state
X0 = fsolve( @(x) hls{2}(0,x,P0(1),P0(2)), X0, optimoptions( 'fsolve', 'Display', 'iter'));

options = odeset( 'RelTol', 1e-8);
%options = odeset('Jacobian',hls(3),'JacobianP',hls(4));
[t,y] = ode45( hls{2}, [0,500], X0, options, P0(1), 40.0);

plot( t, y(:,1));
xlabel( 'Time (ms)');
ylabel( 'Voltage (mV)');
set( gca, 'Fontsize', 20.0);

%% Test Jacobian
if debug
  f = @(x) hls{2}( 0, x, P0(1), P0(2));
  J = feval( hls{3}, 0, X0, P0(1), P0(2));
  I = eye( 5);
  D = zeros( 5);
  epsilon = 1e-4;

  f0 = feval( f, X0);
  for i = 1:5
    D(:,i) = ( feval( f, X0+epsilon*I(:,i)) - f0)/epsilon;
  end

  disp( D);
  disp( J);
end

%% Test JacobianP
if debug
  f = @(p) hls{2}( 0, X0, p(1), p(2));
  J = feval( hls{4}, 0, X0, P0(1), P0(2));
  D = zeros( 5, 2);
  epsilon = 1e-3;

  f0 = feval( f, P0);
  for i = 1:2
    P0(i) = P0(i) + epsilon;
    D(:,i) = ( feval( f, P0) - f0)/epsilon;
    P0(i) = P0(i) - epsilon;
  end

  disp( D);
  disp( J);
end

%% Do continuation in I_app
opt = contset;
opt = contset( opt, 'Singularities', 1);
opt = contset( opt, 'MaxNumPoints', 500000);
opt = contset( opt, 'MaxStepSize', 1);

[x0,v0] = init_EP_EP( @NowackiSimplified, X0, P0, [2]);
[x,v,s,h,f] = cont( @equilibrium, x0, [], opt);

% Plot result
figure;
cpl(x,v,s,[6,1]);grid on;

%% Continue with bigger step-size
% opt = contset( opt, 'Backward', 0);
% opt = contset( opt, 'MaxNumPoints', 10000);
% opt = contset( opt, 'InitStepSize', 0.1);
% % opt = contset( opt, 'MinStepSize', 0.0001);
% opt = contset( opt, 'FunTolerance', 1e-4);
% [x0,v0] = init_EP_EP( @NowackiSimplified, x(1:end-1,end), [P0(1);x(end,end)], [2]);
% 
% [x,v,s,h,f] = cont( @equilibrium, x0, [], opt);
