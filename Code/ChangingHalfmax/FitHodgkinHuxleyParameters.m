% Fit Hodgkin-Huxley parameters
% Kyle Wedgwood
% 21.11.2019

function a1 = FitHodgkinHuxleyParameters( boltzmann_pars, a0, plotSol, V, tau_ref)

  if ~exist( 'V', 'var')
    V = linspace( -80, 50, 1000);
  end
  
  x_inf = 1.0./(1.0+exp(-(V-boltzmann_pars.h)./boltzmann_pars.k));
  if exist( 'tau_ref', 'var')
    res = @(a) FindResidual( [a(1);a(2);a(3);a(4);a(5)], V, x_inf, tau_ref);
  else
    res = @(a) norm( ComputeInactivationPropertiesWrapper( [a(1);a(2);a(3);a(4);a(5)], V) - x_inf);
  end
  
  a1 = fminsearch( res, a0, optimset( 'Display', 'off'));
  
  if ~exist( 'plotSol', 'var')
    plotSol = false;
  end
  
  if plotSol
    fig = figure;
    ax  = axes( fig);
    hold( ax);
    plot( ax, V, x_inf, 'Linewidth', 2.0);
    V = V(1:10:end);
    plot( ax, V, ComputeInactivationPropertiesWrapper( a1, V), ...
      'Linestyle', 'None', 'Marker', 'o');
    
    xlabel( ax, 'Voltage (mV)');
    ylabel( ax, 'x_\infty(V)');
    set( ax, 'Fontsize', 20.0);
    grid on;
    
  end
  
end

function res = FindResidual( a, V, x_inf_ref, tau_ref)
  [x_inf,tau] = ComputeInactivationPropertiesWrapper([a(1);a(2);a(3);a(4);a(5)],V);
  res = norm( x_inf-x_inf_ref + tau-tau_ref);
end