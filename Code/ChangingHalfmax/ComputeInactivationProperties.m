% Function to define inactivation steady state and time constant
% Kyle Wedgwood
% 21.11.2019

function [h_inf,tau] = ComputeInactivationProperties( p, V)

  alpha = p.gain_alpha*exp(-(V-p.h_alpha)/p.k_alpha);
  beta  = 1.0./(1.0 + exp(-(V-p.h_beta)/p.k_beta));
  
  tau = 1.0./(alpha+beta);
  h_inf = alpha.*tau;

end