% Fit Boltzmann parameters
% Kyle Wedgwood
% 21.11.2019

function a1 = FitBoltzmann( V, x_inf, a0, plotSol)

  fun = @(a,v) 1.0./(1.0+exp(-(v-a(1))./a(2)));bolt
  res = @(a) norm( fun( a, V)-x_inf);
  a1 = fminsearch( @(a) res(a), a0, optimset('Display','final'));
  
  if ~exist('plotSol','var')
    plotSol = false;
  end
  
  if plotSol
    figure;
    plot( V, x_inf, V, fun(a1,V));
  end
  
end