% Wrapper for ComputerInactivationProperties
% Kyle Wedgwood
% 21.11.19

function [x_inf,tau] = ComputeInactivationPropertiesWrapper( d, V)

  p.h_alpha = d(1);
  p.k_alpha = d(2);
  p.h_beta  = d(3);
  p.k_beta  = d(4);
  p.gain_alpha = d(5);
  
  [x_inf,tau] = ComputeInactivationProperties( p, V);
  
end