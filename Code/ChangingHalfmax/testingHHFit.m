% Testing fitting wrapper for Hodgkin-Huxley model
% Kyle Wedgwood
% 21.11.19

p0.h_alpha = -65.0;
p0.k_alpha = 20.0;
p0.h_beta  = -35.0;
p0.k_beta  = 10.0;
p0.gain_alpha = 0.07;

a0 = [ p0.h_alpha; p0.k_alpha; p0.h_beta; p0.k_beta; p0.gain_alpha];

% Fit regression line through inactivation data
h = [ -75.0;  % WT
      -62.6;  % D/+
      -58.2]; % D/D
    
k = [ -6.1;  % WT
      -13.5;  % D/+
      -17.6]; % D/D

X = [ ones( 3, 1), h];

beta = X\k;

V = linspace( -100, 20, 1000);
plotSol = false;
[~,tau_ref] = ComputeInactivationPropertiesWrapper( a0, V);

h = linspace( -75, -58.2, 100);
A = zeros( size( a0, 1), length( h));

boltzmann_pars.h = h(1);
boltzmann_pars.k = beta(2)*h(1)+beta(1);

a0 = FitHodgkinHuxleyParameters( boltzmann_pars, a0, true, V, tau_ref);

for i = 1:length( h)
  boltzmann_pars.h = h(i);
  boltzmann_pars.k = beta(2)*h(i)+beta(1);
  close all;
  a1 = FitHodgkinHuxleyParameters( boltzmann_pars, a0, plotSol, V);
  drawnow;
  A(:,i) = a1;
  a0 = a1;
%   [x_inf_ref,tau_ref] = ComputeInactivationPropertiesWrapper( a0, V);
  fprintf( 'Done %d of %d.\n', i, length( h));
end

%% Plot results
fig = figure;
ax  = axes( fig);
hold( ax);
yyaxis( ax, 'left');
plot( ax, h, A(1,:), 'Linewidth', 2.0);
plot( ax, h, A(3,:), 'Linewidth', 2.0);
ylabel( ax, 'h');

yyaxis( ax, 'right');
plot( ax, h, A(2,:), 'Linewidth', 2.0);
plot( ax, h, A(4,:), 'Linewidth', 2.0);
ylabel( ax, 'k');

xlabel( ax, 'p');

set( ax, 'Fontsize', 20.0);
grid on;