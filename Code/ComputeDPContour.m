% Reanalyse data to find depolarisation block contour
% Kyle Wedgwood
% 2.9.2019

function [DP_flag,contour_mat] = ComputeDPContour( final_point, gradient_thresh, g_NaP, g_NaT, I_app)

  hls = feval( @NowackiSimplifiedScaled);
  fun = hls{2};

  no_nap_steps = length( g_NaP);
  no_current_steps = length( I_app);

  DP_flag = zeros( length( g_NaP), length( I_app));
  
  fig = figure( 'Position', [400, 400, 1200, 1200]);
  ax  = axes( fig);
  
  h = imagesc( ax, I_app, g_NaP, DP_flag);
  caxis( ax, [0, 1]);
  xlabel( 'Current density (AU)');
  ylabel( 'g_{NaP} (AU)');
  set( ax, 'YDir', 'normal');
  set( ax, 'XTick', 0:0.1:0.4);
  set( ax, 'YTick', 0:0.1:0.4);
  set( ax, 'Fontsize', 32);
  
  for g_step = 1:no_nap_steps
    for current_step = 1:no_current_steps
      point = final_point{g_step,current_step};
      F = feval( fun, 0, point.state, point.g_NaP, g_NaT, point.I_app);
      if norm( F) < gradient_thresh
        DP_flag(g_step,current_step) = 1;
      end
    end
    fprintf( 'Done %d of %d\n', g_step, no_nap_steps);
  end
  
%   Remove left boundary
  for g_step = 1:no_nap_steps
    row = DP_flag(g_step,:);
    d = diff( row);
    ind = find( d == 1);
    if ~isempty( ind)
      DP_flag(g_step,1:ind(end)) = 0;
    end
  end
  
  % Remove lower boundary
  for g_step = 1:no_nap_steps
    row = DP_flag(g_step,:);
    ind = find( row==1);
    if ~isempty( ind)
      if ind(1) == no_current_steps
        DP_flag(1:g_step-1,:) = 0;
        break
      end
    end
  end

  set( h, 'CData', DP_flag);
  
  contour_mat = contour( I_app, g_NaP, DP_flag, [0.5,0.5]);
  
end