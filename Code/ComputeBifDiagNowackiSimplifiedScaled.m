% Generate bifurcation diagram for NowackiSimplifiedScaled model with I_app
% as control parameter
% DOES NOT CURRENTLY WORK - GETTING STEP-SIZE TOO SMALL ERROR, EVEN WITH
% TINY STEPSIZE
% Kyle Wedgwood
% 17.4.2019

clear; close all; clc;

debug = true;

global cds

run( '~/Dropbox/matcont6p11/init');

X0 = [-0.25;0.1;0.1;0.9;0.9];
P0 = [0.0;130.0;0.5];

hls = feval( @NowackiSimplifiedScaled);

% Start at steady state
X0 = fsolve( @(x) hls{2}(0,x,P0(1),P0(2),P0(3)), X0, optimoptions( 'fsolve', 'Display', 'iter'));

%% Do initial time simulation
options = odeset( 'RelTol', 1e-8);
%options = odeset('Jacobian',hls(3),'JacobianP',hls(4));
[t,y] = ode23( hls{2}, [0,500], X0, options, P0(1), P0(2), P0(3));

plot( t, 100*y(:,1));
xlabel( 'Time (ms)');
ylabel( 'Voltage (mV)');
set( gca, 'Fontsize', 20.0);

%% Test Jacobian
if debug
  f = @(x) hls{2}( 0, x, P0(1), P0(2), P0(3));
  J = feval( hls{3}, 0, X0, P0(1), P0(2), P0(3));
  I = eye( 5);
  D = zeros( 5);
  epsilon = 1e-4;

  f0 = feval( f, X0);
  for i = 1:5
    D(:,i) = ( feval( f, X0+epsilon*I(:,i)) - f0)/epsilon;
  end

  disp( D);
  disp( J);
end

%% Test JacobianP
if debug
  f = @(p) hls{2}( 0, X0, p(1), p(2), p(3));
  J = feval( hls{4}, 0, X0, P0(1), P0(2), P0(3));
  D = zeros( 5, 2);
  epsilon = 1e-3;

  f0 = feval( f, P0);
  for i = 1:2
    P0(i) = P0(i) + epsilon;
    D(:,i) = ( feval( f, P0) - f0)/epsilon;
    P0(i) = P0(i) - epsilon;
  end

  disp( D);
  disp( J);
end

%% Do continuation in I_app
opt = contset;
opt = contset( opt, 'Singularities', 1);
opt = contset( opt, 'Eigenvalues', 1);
opt = contset( opt, 'Backward', 1);
opt = contset( opt, 'MaxNumPoints', 500);
opt = contset( opt, 'InitStepSize', 1e-2);
opt = contset( opt, 'MinStepSize', 1e-20);
opt = contset( opt, 'MaxStepSize', 0.01);
opt = contset( opt, 'FunTolerance', 1e-4);
opt = contset( opt, 'VarTolerance', 1e-4);

[x0,v0] = init_EP_EP( @NowackiSimplifiedScaled, X0, P0, [2]);
[x,v,s,h,f] = cont( @equilibrium, x0, [], opt);

% Plot result
figure;
cpl(x,v,s,[6,1]);grid on;

%% Continue from right Hopf point
x1=x(1:5,s(2).index);
p=[P0(1);x(end,s(2).index)];

opt = contset( opt, 'MaxNumPoints', 1000);
opt = contset( opt, 'InitStepSize', 1e-2);
opt = contset( opt, 'MinStepSize', 1e-5);
opt = contset( opt, 'MaxStepSize', 1.0);
opt = contset( opt, 'Multipliers', 1);

[x0,v0]=init_H_LC( @NowackiSimplifiedScaled, x1, p, [2], 1e-6, 40, 4);
[xlc,vlc,slc,hlc,flc]=cont(@limitcycle,x0,v0,opt);

disp( 'Computed limit-cycle');

%% Process curve
Vlc = xlc(1:5:end-2,:);
max_Vlc = max( Vlc);
min_Vlc = min( Vlc);

% Plot final curve
unstab = any( real( f)>0);

figure(1);
hold on; grid on;

h(1) = plot( x(end,unstab), x(1,unstab), 'Linewidth', 4.0, 'Color', 'red');
h(2) = plot( x(end,~unstab), x(1,~unstab), 'Linewidth', 4.0, 'Color', 'blue');

unstab = any( abs( flc)>1);

h(3) = plot( xlc(end,unstab), min_Vlc(1,unstab), 'Linewidth', 4.0, 'Color', 'red');
h(4) = plot( xlc(end,unstab), max_Vlc(1,unstab), 'Linewidth', 4.0, 'Color', 'red');

h(5) = plot( xlc(end,~unstab), min_Vlc(1,~unstab), 'Linewidth', 4.0, 'Color', 'blue');
h(6) = plot( xlc(end,~unstab), max_Vlc(1,~unstab), 'Linewidth', 4.0, 'Color', 'blue');

xlabel( 'Current density (mA/cm^2)');
ylabel( 'Voltage (mV)');
set( gca, 'XLim', [-0.2,0.4], 'YLim', [-0.8,0.0], 'Fontsize', 20.0, ...
  'XTick', -0.3:0.1:0.4, 'YTick', -0.8:0.2:0.0);

% %% Continue from middle Hopf
% x1=x(1:5,s(5).index);
% p=[P0(1);x(end,s(5).index)];
% 
% opt = contset( opt, 'MaxNumPoints', 1000);
% opt = contset( opt, 'InitStepSize', 1e-2);
% opt = contset( opt, 'MinStepSize', 1e-20);
% opt = contset( opt, 'MaxStepSize', 1.0);
% opt = contset( opt, 'Multipliers', 1);
% opt = contset( opt, 'FunTolerance', 1e-4);
% opt = contset( opt, 'VarTolerance', 1e-4);
% opt = contset( opt, 'MaxCorrIters', 1e2);
% opt = contset( opt, 'MaxNewtonIters', 1e2);
% 
% [x0,v0]=init_H_LC( @NowackiSimplifiedScaled, x1, p, [2], 1e-6, 40, 4);
% [xlc,vlc,slc,hlc,flc]=cont(@limitcycle,x0,v0,opt);
% 
% disp( 'Computed limit-cycle');