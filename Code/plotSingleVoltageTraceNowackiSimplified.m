% Plot single trace of Nowacki simplified model
% Kyle Wedgwood
% 2.9.2019

close all; clc; clear;

X0 = [-0.65;0.1;0.1;0.9;0.9];
g_NaT = 130.0;
g_NaP = 0.4;
I_app = 0.01;

hls = NowackiSimplifiedScaled;

% Start at steady state
X0 = fsolve( @(x) hls{2}( 0, x, g_NaP, g_NaT, 0.0), X0, optimoptions( 'fsolve', 'Display', 'iter'));

% Time options
options = odeset( 'RelTol', 1e-8);
preTime  = 25;
stimTime = 250;
postTime = 25;

% Prepare figure
fig = figure( 'Position', [400, 400, 1200, 800]);
ax = axes( fig, 'Position', [0.1, 0.4, 0.85, 0.55]);
set( ax, 'XLim', [0 preTime + stimTime + postTime]);
set( ax, 'YLim', [-100,60]);
hold( ax);

stim_ax = axes( fig, 'Position', [0.1, 0.1, 0.85, 0.25]);

%% Simulate trajectory
[tPre,yPre] = ode45( hls{2}, [0,preTime], X0, options, g_NaP, g_NaT, 0.0);
XInit = yPre(end,:);

[tStim,yStim] = ode45( hls{2}, tPre(end)+[0,stimTime], XInit, options, g_NaP, g_NaT, I_app);
X0 = yStim(end,:)';

[tPost,yPost] = ode45( hls{2}, tStim(end)+[0,postTime], X0, options, g_NaP, g_NaT, 0.0);

h_pre = plot( ax, tPre, 100*yPre(:,1), 'Linewidth', 5.0, 'Color', 'black');
h_stim = plot( ax, tStim, 100*yStim(:,1), 'Linewidth', 5.0, 'Color', 'black');
h_post = plot( ax, tPost, 100*yPost(:,1), 'Linewidth', 5.0, 'Color', 'black');

ylabel( ax, 'Voltage (mV)');
set( ax, 'Fontsize', 20);

t_total = [tPre; tStim; tPost];
stimulus = [zeros(size(tPre)); I_app*ones(size(tStim)); zeros(size(tPost))];
s_pre = plot( stim_ax, t_total, stimulus, 'Linewidth', 5.0);
x_tick = get( stim_ax, 'XTick');
set( ax, 'XTick', x_tick);
set( ax, 'XTickLabel', []);
set( ax, 'XLim', [0 tPost(end)]);

xlabel( stim_ax, 'Time (ms)');
ylabel( stim_ax, 'Current density (\mu A/cm^2)');
set( stim_ax, 'XLim', [0 tPost(end)]);
set( stim_ax, 'YLim', [0, 0.05]);
set( stim_ax, 'Fontsize', 20);

s = input( 'Press ENTER to save figure', 's');
axis off;

if isempty( s)
  filename = sprintf( '../Figures/voltageTrace_gnat_%.0f_gnap_%.2f_Iapp_%.2f.eps', ...
    g_NaT, g_NaP, I_app);
  saveas( fig, filename, 'epsc')
  fprintf( 'Figure saved as %s.\n', filename);
end