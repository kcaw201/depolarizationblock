% Find boundary of when to expect depolarisation block (either in short or
% long times)
% 17.08.2020
% Kyle Wedgwood
% 
% 17.08.2020 DEFUNCT - replaced by
% DDEBifTool/findDepolarisationBoundaryTwoPar.m

% Model defintion (for hfo only)
addpath('~/Dropbox/MatCont7p2/Systems/');

temp = NowackiSimplifiedScaledSlow1;

fun = temp{2};

% Parameters
I   = 0.2;
gti = 130.0;
gpi = 0.0;

optopts = optimset('Display', 'none');

hfo = findSteadyState(fun, I, gti, gpi, optopts);

% Initial condition for hfo
hfo_0 = findSteadyState(fun, 0, gti, gpi, optopts);
hfo = find500msSol(fun, I, gti, gpi, hfo_0);

% Function to find steady state value
function hfo = findSteadyState(fun, I, gti, gpi, options)

  hfo = fzero(@(hfo) fun(0, hfo, I, gti, gpi), [0,1], options);

end

% Function to find solution after 500 ms (using bvp solver)
function hfo = find500msSol(fun, I, gti, gpi, hfo_0, solinit)

  boundaryConditions = @(ya,yb) ya-hfo_0;
  
  if nargin < 6
    time = linspace(0, 500, 5);
    initialGuess = @(t) hfo_0;
    solinit = bvpinit(time, initialGuess);
  end
  
  bvpopts = bvpset('AbsTol', 1e-12, 'RelTol', 1e-8, 'Vectorized', true);
  
  sol = bvp4c(@(t,y) fun(t, y, I, gti, gpi), ...
              @(ya,yb) boundaryConditions(ya,yb), solinit, bvpopts);
            
  hfo = sol.y(end);
            
end
