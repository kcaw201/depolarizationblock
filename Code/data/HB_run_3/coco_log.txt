MATLAB version 9.0.0.341360 (R2016a) on architecture maci64

run: { runid='HB_run_3' toolbox='empty' tbxctor=@empty_ctor isol_type='' sol_type='' dir='/Users/kcaw201/Dropbox/DepolarizationBlock/Code/data/HB_run_3' bdfname='/Users/kcaw201/Dropbox/DepolarizationBlock/Code/data/HB_run_3/bd.mat' logname='/Users/kcaw201/Dropbox/DepolarizationBlock/Code/data/HB_run_3/coco_log.txt' scrname='/Users/kcaw201/Dropbox/DepolarizationBlock/Code/data/HB_run_3/coco_scr.txt' }

all: { TOL=1e-06 CleanData=false LogLevel=1 data_dir='/Users/kcaw201/Dropbox/DepolarizationBlock/Code/data' }
  +-ode: { vectorized=false }
  +-lsol: { det=true }

funcs
  +-ep: { type='zero' }
  | +-pars: { type='inactive' pars={ 'I_app' 'g_pi' 'g_ti' } }
  | +-var: { type='zero' }
  | +-HB: { type='zero' }
  | +-test: { type='regular' pars={ 'ep.test.BTP' } }
  +-hb_glue: { type='zero' }
  +-cseg
  | +-prcond: { type='zero' }
  +-atlas
    +-test
      +-BP: { type='singular' pars={ 'atlas.test.BP' } }
      +-FP: { type='singular' pars={ 'atlas.test.FP' } }

slots
  +-ep: { signals={ 'bddat@bddat' 'save_full@coco_save_data' } }
  | +-HB: { signal='update@update' }
  +-cseg
  | +-fix_mfunc: { signal='fix_mfunc@CurveSegmentBase.fix_mfunc' }
  | +-remesh: { signal='remesh@CurveSegmentBase.remesh' }
  | +-set_mode: { signal='set_mode@CurveSegmentBase.set_mode' }
  | +-update: { signal='update@CurveSegmentBase.update' }
  | +-update_h: { signal='update_h@CurveSegmentBase.update_h' }
  | +-update_w: { signal='update_w@CurveSegmentBase.update_w' }
  +-tb_info: { signals={ 'save_bd@coco_save_data' 'save_reduced@coco_save_data' } }
  +-run: { signal='save_bd@save_run' }
  +-bd: { signal='save_bd@save_bd' }
  +-bddat: { signal='save_bd@save_bddat' }
  +-lsol
    +-det: { signal='set_mode@set_save_det' }

signals
  +-save_bd: { owner='coco' slots={ 'tb_info@coco_save_data' 'run@save_run' 'bd@save_bd' 'bddat@save_bddat' } }
  +-update: { owner='CurveSegmentBase' slots={ 'ep.HB@update' 'cseg.update@CurveSegmentBase.update' } }
  +-set_mode: { owner='CurveSegmentBase' slots={ 'cseg.set_mode@CurveSegmentBase.set_mode' 'lsol.det@set_save_det' } }
  +-update_h: { owner='CurveSegmentBase' slots={ 'cseg.update_h@CurveSegmentBase.update_h' } }
  +-fix_mfunc: { owner='CurveSegmentBase' slots={ 'cseg.fix_mfunc@CurveSegmentBase.fix_mfunc' } }
  +-update_w: { owner='atlas_1d' slots={ 'cseg.update_w@CurveSegmentBase.update_w' } }
  +-remesh: { owner='atlas_1d' slots={ 'cseg.remesh@CurveSegmentBase.remesh' } }
  +-bddat: { owner='AtlasBase' slots={ 'ep@bddat' } }
  +-save_full: { owner='AtlasBase' slots={ 'ep@coco_save_data' } }
  +-save_reduced: { owner='AtlasBase' slots={ 'tb_info@coco_save_data' } }

*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          4.09e-05  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  3.90e-05  2.13e-09  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  9.22e-09  1.13e-08  2.80e+02    0.0    0.0    0.0
atlas_1d: init_chart: angle(t,t30) =  6.40e+01[DEG]
atlas_1d: init_chart: angle(t,t31) =  6.28e+01[DEG]

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    0    1.00e-01  00:00:00   2.8007e+02      1  EP      2.2726e-01   3.9812e-06

POINT 0: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          3.90e-04  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  3.75e-04  5.08e-09  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.82e-08  2.29e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    1    1.90e-01  00:00:00   2.8007e+02                 1.8327e-01   4.5747e-02

POINT 1: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          2.29e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          1.38e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  1.33e-03  4.89e-08  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.26e-07  3.48e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    2    3.61e-01  00:00:00   2.8007e+02                 9.9285e-02   1.3313e-01

POINT 2: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          3.48e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          4.81e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  4.63e-03  6.26e-07  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.86e-06  7.53e-09  2.80e+02    0.0    0.0    0.0
   3   1  1.00e+00  2.39e-08  8.41e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    3    5.00e-01  00:00:00   2.8007e+02                -6.1672e-02   3.0076e-01

POINT 3: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          8.41e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          8.67e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  8.39e-03  2.25e-06  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  6.48e-06  6.82e-09  2.80e+02    0.0    0.0    0.0
   3   1  1.00e+00  3.01e-08  1.12e-08  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    4    5.00e-01  00:00:00   2.8008e+02                -2.8730e-01   5.3604e-01

POINT 4: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          1.12e-08  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          8.01e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  7.81e-03  2.17e-06  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  6.01e-06  1.12e-08  2.80e+02    0.0    0.0    0.0
   3   1  1.00e+00  1.46e-08  8.56e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          7.97e-05  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  1.74e-04  9.33e-09  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.13e-08  6.25e-10  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    5    5.00e-01  00:00:00   2.8008e+02      2  EP     -5.0000e-01   7.5808e-01

POINT 5: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          3.93e-04  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  3.80e-04  3.79e-09  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.04e-08  4.64e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    0    1.00e-01  00:00:00   2.8007e+02      3  EP      2.2726e-01   3.9812e-06
    1    1.90e-01  00:00:00   2.8006e+02                 2.7109e-01  -4.5562e-02

POINT 1: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          4.64e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          1.44e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  1.40e-03  4.80e-08  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.47e-07  2.14e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    2    3.61e-01  00:00:00   2.8006e+02                 3.5395e-01  -1.3163e-01

POINT 2: computation finished
*********************************************************************

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          2.14e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          5.40e-03  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  5.23e-03  6.78e-07  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  1.84e-06  3.29e-09  2.80e+02    0.0    0.0    0.0
   3   1  1.00e+00  1.19e-08  2.43e-09  2.80e+02    0.0    0.0    0.0

    STEP   DAMPING               NORMS              COMPUTATION TIMES
  IT SIT     GAMMA     ||d||     ||f||     ||U||   F(x)  DF(x)  SOLVE
   0                          6.81e-05  2.80e+02    0.0    0.0    0.0
   1   1  1.00e+00  1.59e-04  1.15e-08  2.80e+02    0.0    0.0    0.0
   2   1  1.00e+00  2.40e-08  8.40e-09  2.80e+02    0.0    0.0    0.0

 STEP   STEP SIZE      TIME        ||U||  LABEL  TYPE         I_app         g_pi
    3    5.00e-01  00:00:00   2.8006e+02      4  EP      5.0000e-01  -2.8315e-01

POINT 3: computation finished
*********************************************************************
