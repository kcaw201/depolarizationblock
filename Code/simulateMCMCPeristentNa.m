% Simulate Markov chain model for persistent Na+ channel with blocked state
% Kyle Wedgwood
% 7.7.2020

% Rate functions
alpha_m = @(v) 0.1*(v+40)/(1.0-exp(-0.1*(v+40)));
beta_m  = @(v) 4.0*exp(-0.0556*(v+65));
alpha_h = @(v) 0.07*exp(-0.05*(v+65));
beta_h  = @(v) 1.0/(1.0+exp(-0.1*(v+35)));

N = 10000;

% Initial conditons
index = 1;
t = 0.0;
n = [ N; zeros(8, 1)];
% n = randi(1000,9,1);

stim_on_time = 200.0;
tfinal = 2000.0;

buffer_size = 1000000;
T_array = zeros(buffer_size, 1);
N_array = zeros(9, buffer_size);
I_array = zeros(buffer_size, 1);

v = -65.0;
v_Na = 50.0;
g_Na = 1.0;

alpha_p = 1.0;
beta_p = 0.1;

% State transition - note that this is transposed
transition = [ -1  1  0  0  0  0  0  0  0;
                0 -1  1  0  0  0  0  0  0;
                0  0 -1  1  0  0  0  0  0;
                0  0  0  0 -1  1  0  0  0;
                0  0  0  0  0 -1  1  0  0;
                0  0  0  0  0  0 -1  1  0;
                1 -1  0  0  0  0  0  0  0;
                0  1 -1  0  0  0  0  0  0;
                0  0  1 -1  0  0  0  0  0;
                0  0  0  0  1 -1  0  0  0;
                0  0  0  0  0  1 -1  0  0;
                0  0  0  0  0  0  1 -1  0;
               -1  0  0  0  1  0  0  0  0;
                0 -1  0  0  0  1  0  0  0;
                0  0 -1  0  0  0  1  0  0;
                0  0  0 -1  0  0  0  1  0;
                1  0  0  0 -1  0  0  0  0;
                0  1  0  0  0 -1  0  0  0;
                0  0  1  0  0  0 -1  0  0;
                0  0  0  1  0  0  0 -1  0;
                0  0  0 -1  0  0  0  0  1;
                0  0  0  1  0  0  0  0 -1]';
                

while t < tfinal
    
    if t > stim_on_time
        v = -20.0;
    end
    
    % Store current point
    T_array(index) = t;
    N_array(:,index) = n;
    I_array(index) = g_Na*(n(4)+n(9))*(v_Na-v);
    
    % Define probabilities
    a_m = alpha_m(v);
    b_m = beta_m(v);
    a_h = alpha_h(v);
    b_h = beta_h(v);
    p = [ 3*a_m*n(1); 2*a_m*n(2); a_m*n(3);
          3*a_m*n(5); 2*a_m*n(6); a_m*n(7);
          b_m*n(2); 2*b_m*n(3); 3*b_m*n(4);
          b_m*n(6); 2*b_m*n(7); 3*b_m*n(8);
          b_h*n(1:4); a_h*n(5:8); alpha_p*n(4); beta_p*n(9)];
    lambda = sum(p);
         
    u = rand;
    time_to_next_transition = -log(u)/lambda;
    t = t + time_to_next_transition;
    
    % Now select transition
    boundaries = [0;cumsum(p)/lambda];
    u = rand;
    ind = find((u > boundaries(1:end-1)) & (u < boundaries(2:end)));
    
    n = n + transition(:,ind);
    if any(n < 0)
        pause;
    end
    
    T_array(index+1) = t;
    N_array(:,index+1) = n;
    I_array(index+1) = g_Na*(n(4)+n(9))*(v_Na-v);
    
    index = index+2;
    
end

% Plot figure
fig = figure();
ax = axes(fig);
plot(ax, T_array, I_array, 'Linewidth', 4.0);
xlabel(ax, 'Time (ms)');
ylabel(ax, 'Current density');
set(ax, 'Fontsize', 20);