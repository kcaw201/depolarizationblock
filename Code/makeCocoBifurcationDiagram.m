clear; close all; clc;

run( '~/Dropbox/coco/startup');

g_NaT = 130.0;
g_NaP = 0.2;

p = [ 0.0; g_NaP; g_NaT];

funs = feval( @NowackiSimplifiedScaled);
ode_fun  = @(x,p) funs{2}(0,x,p(2),p(3),p(1));
jac_fun  = @(x,p) funs{3}(0,x,p(2),p(3),p(1));
jacp_fun = @(x,p) funs{4}(0,x,p(2),p(3),p(1));

prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);

y0 = [-0.65;0.1;0.1;0.8;0.8];
x0 = fsolve( @(x) ode_fun(x,p), y0);

ode_funs = { ode_fun, jac_fun, jacp_fun};

label_root = sprintf( 'g_NaP%0.2f_', g_NaP);

prob = ode_isol2ep(prob, '', ode_funs{:}, x0, {'I_app', 'g_pi', 'g_ti'}, [0.0; 0.0; 280.0]);
ep_label = strcat( label_root, 'run1');
bd = coco(prob, ep_label, [], 1, {'I_app','g_pi', 'g_ti'}, [-0.2 0.5]);

HB_labs = coco_bd_labs( bd, 'HB');
SN_labs = coco_bd_labs( bd, 'SN');

%% Plot figure
X = coco_bd_col( bd,'x');
I = coco_bd_col( bd, 'I_app');

coco_plot_bd( ep_label, 'I_app', 'x');

ax = gca;
hold( ax);
set( ax, 'Fontsize', 20.0);
set( ax, 'XLim', [-0.1 0.5], 'YLim', [-1,0.0]);

%% Continue periodic orbits
prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
prob = coco_set( prob, 'cont', 'h0', 1e-4, 'h_min', 1e-4, 'h_max', 1e-1, 'ItMX', 10000, 'NAdapt', 1, 'NTST', 20);
prob = coco_set( prob, 'corr', 'TOL', 1e-4, 'ItMX', 1000, 'SubItMx', 100, 'TOL', 1e-6, 'ResTOL', 1e-6);
% prob = coco_set( prob, 'po', 'bifus', false);
prob = ode_HB2po(prob, '', 'run1', HB_labs(3));
po_3_label = strcat( label_root, 'po_run_3');
bd_po_3 = coco(prob, po_3_label, [], 1, 'I_app', [-0.5 0.5]);

%%
prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
prob = coco_set( prob, 'cont', 'h0', 1e-4, 'h_min', 1e-6, 'h_max', 1e-2, 'ItMX', 10000, 'NAdapt', 1, 'NTST', 20);
prob = coco_set( prob, 'corr', 'TOL', 1e-4, 'ItMX', 1000, 'SubItMx', 100, 'TOL', 1e-6, 'ResTOL', 1e-6);
prob = ode_HB2po(prob, '', 'run1', HB_labs(1));
po_1_label = strcat( label_root, 'po_run_1');
bd_po_1 = coco(prob, po_1_label, [], 1, 'I_app', [-0.5 0.5]);

%% Plot results
coco_plot_bd( ep_run, 'I_app', 'x');

ax = gca;
hold( ax);  
set( ax, 'Fontsize', 20.0);
set( ax, 'XLim', [-0.1 0.5], 'YLim', [-1,0.5]);
coco_plot_bd( po_1_label, 'I_app', 'MAX(x)');
coco_plot_bd( po_1_label, 'I_app', 'MIN(x)');
coco_plot_bd( po_3_label, 'I_app', 'MAX(x)');
coco_plot_bd( po_3_label, 'I_app', 'MIN(x)');

%% Continue Hopf points
prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
% prob = coco_set( prob, 'corr', 'TOL', 1e-4, 'ItMX', 1000, 'SubItMx', 100);
prob = ode_HB2HB(prob, '', 'run1', HB_labs(1));
HB_1_label = strcat( label_root, 'HB_run_1');
bd2 = coco(prob, HB_1_label, [], 1, {'I_app', 'g_pi'}, [-0.5 0.5]);

prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
prob = ode_HB2HB(prob, '', 'run1', HB_labs(2));
HB_2_label = strcat( label_root, 'HB_run_2');
bd3 = coco(prob, HB_2_label, [], 1, {'I_app', 'g_pi'}, [-0.5 0.5]);

prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
prob = ode_HB2HB(prob, '', 'run1', HB_labs(3));
HB_3_label = strcat( label_root, 'HB_run_3');
bd4 = coco(prob, HB_3_label, [], 1, {'I_app', 'g_pi'}, [-0.5 0.5]);

%% Continue saddle node points
prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
% prob = coco_set( prob, 'corr', 'TOL', 1e-4, 'ItMX', 1000, 'SubItMx', 100);
prob = ode_SN2SN(prob, '', 'run1', SN_labs(1));
SN_1_label = strcat( label_root, 'SN_run_1');
bd5 = coco(prob, SN_1_label, [], 1, {'I_app', 'g_pi'}, [-0.5 0.5]);

prob = coco_prob();
prob = coco_set( prob, 'ode', 'vectorized', false);
prob = ode_SN2SN(prob, '', 'run1', SN_labs(2));
SN_2_label = strcat( label_root, 'SN_run_2');
bd6 = coco(prob, SN_2_label, [], 1, {'I_app', 'g_pi'}, [-0.5 0.5]);

%% Plot results
figure;
ax = gca;
hold( ax);
coco_plot_bd( HB_1_label, 'I_app', 'g_pi');
coco_plot_bd( HB_2_label, 'I_app', 'g_pi');
coco_plot_bd( HB_3_label, 'I_app', 'g_pi');
coco_plot_bd( SN_1_label, 'I_app', 'g_pi');
coco_plot_bd( SN_2_label, 'I_app', 'g_pi');
