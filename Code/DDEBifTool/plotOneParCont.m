% Plot one parameter branch with trajectory
% 16.9.2020
% Kyle Wedgwood

close all; clear; clc;

% Load data
eq_branch_name  = 'eq_hfo_branch.mat';
LC_branch_name  = 'LC_hfo_branch.mat';

load(eq_branch_name);
load(LC_branch_name);

% Plot variables
lw = 2;
mso_ind = 3;
hfo_ind = 5;
plot_ind = hfo_ind;
V_ind   = 1;
time_ind = 6;

figure('Position', [200, 200, 800, 600]);
subplot(1, 1, 1);
[xm,ym] = df_measr(0, stst_branch, 'stst');
br_splot(stst_branch, xm, ym);

[xm,ym] = df_measr(0, LC_branch, 'stst');
ym.col = 'max';
br_splot(LC_branch, xm, ym);
ym.col = 'min';
br_splot(LC_branch, xm, ym);

% Find bifurcation point
nunst = GetStability(LC_branch);

% Resize lines
lines = findobj(gca, 'type', 'line');

for i = 1:length(lines)
  set(lines(i), 'Linewidth', lw);
end

% Set markers to all be red
markers = findobj(gca, 'type', 'line', 'marker', '*');
set(markers(1), 'Color', 'red', 'Markersize', 50, 'Marker', '.');
set(markers(2), 'Color', 'red', 'Markersize', 50, 'Marker', '.');
% set(markers(3), 'Color', 'black', 'Markersize', 50, 'Marker', '.');

markers = findobj(gca, 'type', 'line', '-not', 'marker', 'none');
for i = 1:length(markers)
  uistack(markers(i), 'top');
end

% Add dashed line for bifurcation
hfo_fold = get(markers(1), 'XData');
h = line(hfo_fold*[1,1], [-0.8,0.8], 'Linestyle', '--', 'Color', 'red', ...
        'Linewidth', 1);
uistack(h, 'bottom');

markers = findobj(gca, 'type', 'line', 'marker', 'x');
for i = 1:length(markers)
  set(markers(i), 'Color', 'blue', 'Markersize', 50, 'Marker', '.');
end

% Overlay trajetory
pars = LC_branch.point(1).parameter(1:3);

T = 1000;
h1 = plotSingleSimulation(pars, T, plot_ind, V_ind, gca);

par_labels = {'I (mA/cm^2)', 'g_{NaT} (mS/cm^2)', 'g_{NaP} (mS/cm^2)'};
var_labels = {'Voltage (mV)', 'm_{fo}', 'm_{so}', 'h_{ti}', 'h_{fo}', 'Time (ms)'};
          
xlabel(var_labels{hfo_ind});
ylabel(var_labels{V_ind});
set(gca, 'Fontsize', 32);
set(gca, 'XLim', [0.2, 1.0]);
set(gca, 'YLim', [-0.8, 0.6]);
VTick = -0.8:0.4:0.4;
hTick = 0:0.4:0.8;
set(gca, 'XTick', hTick);
set(gca, 'YTick', VTick, 'YTickLabels', 100*VTick);
box(gca, 'off');

title(sprintf('I_{app} = %0.2f, g_{NaT} = %0.0f, g_{NaP} = %0.2f', ...
              pars(1), pars(2), pars(3)), 'Fontsize', 24);

% Add trajectory axis
ins_ax = axes(gcf, 'Position', [0.7,0.5, 0.2, 0.16]);
h1 = plotSingleSimulation(pars, T, time_ind, V_ind, ins_ax);
hold(ins_ax, 'on');
set(h1, 'Linewidth', 0.1);
xlabel(var_labels{time_ind});
ylabel(var_labels{V_ind});
xlim(ins_ax, [0, T]);
VTick = -0.8:0.4:0.4;
tTick = [0,500,1000];
set(ins_ax, 'XTick', tTick);
set(ins_ax, 'YTick', VTick, 'YTickLabels', 100*VTick);

yyaxis right;
h2 = plotSingleSimulation(pars, T, time_ind, hfo_ind, ins_ax);
ylim(ins_ax, [0,1]);
ylab = ylabel(ins_ax, var_labels{hfo_ind}, 'Rotation', 0, ...
              'Position', [1120,0.6]);
line(ins_ax, [0,T], hfo_fold*[1,1], 'Linestyle', '--', ...
     'Color', get(ylab, 'Color'), 'Linewidth', 2);  
set(h2, 'Linewidth', 2, 'Color', get(ylab, 'Color'));
set(ins_ax, 'YTick', [0,1]);
set(ins_ax, 'Fontsize', 16);

% exportgraphics(gcf, '../../Figures/hfo_bifDiagNoBlock.eps');