% Finding asymptotic depolaristion block boundary using 
% ddebiftool (to impose additional constraints)
% 19.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
load_branches = false;
save_branches = false;
eq_branch_name  = 'eq_hfo_branch.mat';
LC_branch_name  = 'LC_hfo_branch.mat';
DP_branch_name  = 'DP_hfo_stst_branch.mat';

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

dummy_tau = @()[6];

funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled, ...
  'sys_tau', dummy_tau);

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  
  delay = 0.0; % don't include a delay
  
  % Parameters
  Iapp = 0.35;
  gti  = 130.0;
  gpi  = 0.0;
  hfo  = 0.0;
  init = [-0.276; 0.129; 0.56; 0.0023];
  par  = [Iapp,gti,gpi,hfo,500,delay];
  
  % Find starting point
  init = fsolve(@(x) funcs.sys_rhs(x,par), init);

  % Use utility functions to get us started
  [stst_branch,suc] = SetupStst(funcs, 'x', init, ...
                                'parameter', par, ...
                                'min_bound', [1 0; 2 0; 3 0; 4 0; 5 0], ...
                                'max_bound', [3 1; 4 1], ...
                                 'contpar', 4);

  % Continue equilibrium branch
  stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);
  if save_branches
    save(eq_branch_name, 'stst_branch');
  end
  
  % Plot branch
  [xm,ym] = df_measr(0, stst_branch, 'x');
  figure;
  br_splot(stst_branch, xm, ym);
end

%% Continue solution at Hopf
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Branch off at Hopf bifurcation');
  ind_hopf = find(nunst_eqs == 2, 1, 'first');
  fprintf('Initial correction of periodic orbits at Hopf:\n');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_hopf, ...
                             'intervals',20,'degree',4);

  hold on;
  LC_branch = br_contn(funcs, LC_branch, 300);
  LC_branch = br_stabl(funcs, LC_branch, 0, 1);
  
  %% Plot stability
  [xm,ym] = df_measr(0, LC_branch, 'psol');
  ym.col = 'max';
  br_splot(LC_branch, xm, ym)

  ym.col = 'min';
  br_splot(LC_branch, xm, ym);
  
  if save_branches
    save(LC_branch_name, 'LC_branch');
  end
end

%% Setup PO fold continuation
if exist(DP_branch_name, 'file') && load_branches
  load(DP_branch_name);
else
  nunst_per = GetStability(LC_branch, 'exclude_trivial', true);
  disp('Continue branch of POfold');
  ind_LPC = find(nunst_per == 2, 1, 'first');
  fprintf('Initial correction of PO fold:\n');
  
  figure;
  funcs.sys_cond = @DPcondStSt;
  [fold_funcs,DP_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
    'contpar', [1,3,4],...
    'dir', 4);
  
  DP_branch.method.continuation.steplength_growth_factor = 1.0;
  DP_branch = br_contn(fold_funcs, DP_branch, 100);
  
  %% Check that point is on DP boundary
  if debug
    for i = 1:length(DP_branch.point)
      I_curr    = DP_branch.point(i).parameter(1);
      gti_curr  = DP_branch.point(i).parameter(2);
      gpi_curr  = DP_branch.point(i).parameter(3);
      hfo_curr  = DP_branch.point(i).parameter(4);
      T_curr    = DP_branch.point(i).parameter(5);
      
      hfo = findHFOT(fun, I_curr, gti_curr, gpi_curr, T_curr);
      fprintf('Point: %d, Target hfo: %f, actual hfo: %f\n', i, hfo_curr, hfo);
    end
  end
  
  %% Plot branch
  figure;
  [xm,ym] = df_measr(0, DP_branch, 'psol');
  ym.col = 5;
  br_plot(DP_branch, xm, ym);
  
  if save_branches
    save(DP_branch_name, 'DP_branch');
  end
end