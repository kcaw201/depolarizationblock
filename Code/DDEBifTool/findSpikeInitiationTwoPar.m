% Finding spiking initiation branch in (I, gpi) space using
% ddebiftool (to impose additional constraints)
% 19.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
load_branches = false;
save_branches = false;
eq_branch_name  = 'eq_mso_branch.mat';
eq2_branch_name  = 'eq_mso_branch.mat';
hopf_branch_name = 'hopf_mso_branch.mat';
fold_branch_name = 'fold_mso_branch.mat';

LC_branch_name  = 'LC_mso_branch.mat';
LPC_branch_name  = 'LPC_mso_branch.mat';
temp_branch_name  = 'temp_mso_stst_branch.mat';

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

T = 500.0;

dummy_tau = @()[7];

funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled2, ...
  'sys_tau', dummy_tau);

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  delay = 0.0; % don't include a delay

  % Parameters
  Iapp = 0.16;
  gti  = 130.0;
  gpi  = 0.35;
  mso  = 0.0;
  hfo  = 0.7128;

  init = [-0.2766; 0.129; 0.0023];
%   init = [-0.766; 0.129; 0.0023]; % check to see if there are two steady state branches
  par  = [Iapp,gti,gpi,mso,hfo,T,delay];

  % Find starting point
  init = fsolve(@(x) funcs.sys_rhs(x,par), init);

  % Use utility functions to get us started
  [stst_branch,suc] = SetupStst(funcs, 'x', init, ...
                                'parameter', par, 'contpar', 4);
%                                 'min_bound', [1 -1; 2 0; 3 -1; 4 -1; 5 -1; 6 0], ...
%                                 'max_bound', [3 1; 4 1; 5 1], ...
%                                 'contpar', 4);

  % Continue equilibrium branch
  figure(101);
  stst_branch = br_contn(funcs, stst_branch, 20);
  stst_branch = br_rvers(stst_branch);
  stst_branch.method.continuation.steplength_growth_factor = 1.0;
%   stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);
  if save_branches
    save(eq_branch_name, 'stst_branch');
  end

  %% Plot branch
  [xm,ym] = df_measr(0, stst_branch, 'x');
  figure;
  br_splot(stst_branch, xm, ym);
end

%% Continue periodic orbit
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Continuing periodic orbit');
  ind_hopf = find(abs(diff(nunst_eqs)) == 2, 1, 'last');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_hopf, ...
                              'intervals', 20, 'degree', 4);

   LC_branch.method.continuation.steplength_growth_factor = 1.1;
   LC_branch.method.point.extra_condition = 1;

   figure(101);
   LC_branch = br_contn(funcs, LC_branch, 50);
   LC_branch = br_stabl(funcs, LC_branch, 0, 1);

   %% Plot solution
   figure(99);
   [xm,ym] = df_measr(0, LC_branch, 'psol');
   br_splot(LC_branch, xm, ym);
   xlabel('m_{so}');
   ylabel('V');
   y_lim = get(gca, 'YLim');
   ylim([0,y_lim(2)]);

  if save_branches
    save(LC_branch_name, 'LC_branch');
  end

end

%% Continue fold of periodic orbit
if exist(LPC_branch_name, 'file') && load_branches
  load(LPC_branch_name);
else
  nunst_per = GetStability(LC_branch);
  nunst_per = nunst_per(2:end); % first point is degenerate
  disp('Continuing fold of periodic orbit');
  funcs.sys_cond = @SIcond0;
  ind_LPC = find(nunst_per == 0, 1, 'first') + 1;

  [fold_funcs,LPC_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
    'contpar', [1,3,4,5], 'dir', [1,3,4], 'step', 0.0001);

  LPC_branch.method.continuation.steplength_growth_factor = 1.1;
  LPC_branch.method.point.extra_condition = 1;

  figure(101); cla;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);
  LPC_branch = br_rvers(LPC_branch);
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);

  %% Plot solution
  figure(100);
  [xm,ym] = df_measr(0, LPC_branch, 'psol');
  br_plot(LPC_branch, xm, ym);
  xlabel('I');
  ylabel('g_{NaP}');
  y_lim = get(gca, 'YLim');
   ylim([0,y_lim(2)]);

  if save_branches
    save(LPC_branch_name, 'LPC_branch');
  end

end

%% Continue Hopf branch
if exist(hopf_branch_name, 'file') && load_branches
  load(hopf_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Continuing Hopf bifurcation');
  funcs.sys_cond = @SIcond0;
  stst_branch.method.point.extra_condition = 1;
  ind_hopf = find(abs(diff(nunst_eqs)) == 2, 1, 'last');
  [hopf_branch,suc] = SetupHopf(funcs, stst_branch, ind_hopf, ...
                                'contpar', [1,3,4,5], ...
                                'dir', 1, 'step', 1e-3);

   hopf_branch.method.continuation.steplength_growth_factor = 1.1;
   hopf_branch.method.point.extra_condition = 1;

   hopf_branch = br_contn(funcs, hopf_branch, 100);
   hopf_branch = br_rvers(hopf_branch);
   hopf_branch = br_contn(funcs, hopf_branch, 100);
   hopf_branch = br_stabl(funcs, hopf_branch, 0, 1);

   %% Plot solution
   figure(99);
   [xm,ym] = df_measr(0, hopf_branch, 'hopf');
   br_plot(hopf_branch, xm, ym);
   xlabel('I');
   ylabel('g_{Nap}');
   y_lim = get(gca, 'YLim');
   ylim([0,y_lim(2)]);

  if save_branches
    save(hopf_branch_name, 'hopf_branch');
  end

end

%% Now repeat for fold branch
if exist(eq2_branch_name, 'file') && load_branches
  load(eq2_branch_name);
else
  delay = 0.0; % don't include a delay

  % Parameters
  Iapp = 0.08;
  gti  = 130.0;
  gpi  = 0.2;
  mso  = 0.0;
  hfo  = 0.72;

  init = [-0.2766; 0.129; 0.0023];
%   init = [-0.766; 0.129; 0.0023]; % check to see if there are two steady state branches
  par  = [Iapp,gti,gpi,mso,hfo,T,delay];

  % Find starting point
  init = fsolve(@(x) funcs.sys_rhs(x,par), init);

  % Use utility functions to get us started
  [stst_branch,suc] = SetupStst(funcs, 'x', init, ...
                                'parameter', par, 'contpar', 4);
%                                 'min_bound', [1 -1; 2 0; 3 -1; 4 -1; 5 0; 6 0], ...
%                                 'max_bound', [3 10; 4 10; 5 1], ...
%                                 'contpar', 4);

  % Continue equilibrium branch
  stst_branch.method.continuation.steplength_growth_factor = 1.0;
  stst_branch = br_contn(funcs, stst_branch, 60);
  stst_branch = br_rvers(stst_branch);
%   stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);

  % Plot branch
  [xm,ym] = df_measr(0, stst_branch, 'x');
  figure;
  br_splot(stst_branch, xm, ym);

  if save_branches
    save(eq_branch_name, 'stst_branch');
  end
end

if exist(fold_branch_name, 'file') && load_branches
  load(fold_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Continuing fold bifurcation');
  funcs.sys_cond = @SIcond0;
  stst_branch.method.point.extra_condition = 1;
  ind_fold = find(abs(diff(nunst_eqs)) == 1, 1, 'first');
  [fold_branch,suc] = SetupFold(funcs, stst_branch, ind_fold, ...
                                'contpar', [1,3,4,5], ...
                                'dir', 1, 'step', 1e-3);

   fold_branch.method.continuation.steplength_growth_factor = 1.1;
   fold_branch.method.point.extra_condition = 1;

   figure(101); cla;
   fold_branch = br_contn(funcs, fold_branch, 200);
   fold_branch = br_rvers(fold_branch);
   fold_branch = br_contn(funcs, fold_branch, 20);

   %% Plot solution
   figure(99); cla;
   [xm,ym] = df_measr(0, fold_branch, 'fold');
   br_plot(fold_branch, xm, ym);
   xlabel('I');
   ylabel('g_{Nap}');
   y_lim = get(gca, 'YLim');
   xlim([0,0.4]);
   ylim([0,0.4]);

   if save_branches
     save(fold_branch_name, 'fold_branch');
   end
end
