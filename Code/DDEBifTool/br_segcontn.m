% Segmented continuation of branch (in case errors are thrown during
% evaluation of rhs)
% Code will attempt to produce an overall branch with no_segs x no_pts
% overall points in chunks of no_pts
% If an exception is thrown during evaluation of one of the segments, the
% rest will be returned
% Parameter bounds are checked after each segment is computed to ensure
% consistency with usual use of br_contn (i.e. repeated segments will not 
% be computed if the end point of one branch is at a parameter bound)
% 22.9.2020
%
% INPUTS:  funcs   - rhs to be evaluated
%          branch  - existing branch to be continued
%          no_segs - number of branch segments to be computed
%          no_pts  - number of points per branch segment

% OUTPUT:  branch  - extended branch
%
% Kyle Wedgwood

function branch = br_segcontn(funcs, branch, no_segs, no_pts)

  stop_flag = false;
  
  for j = 1:no_segs
    try
      branch = br_contn(funcs, branch, no_pts);
    catch
      break;
    end
    
    % Check if any boundary conditions are met
    pars = branch.point(end).parameter;
    
    bounds = branch.parameter.min_bound;
    for i = 1:length(bounds)
      if pars(bounds(i,1)) <= bounds(i,2)
        stop_flag = true;
      end
    end   
    
    bounds = branch.parameter.max_bound;
    for i = 1:length(bounds)
      if pars(bounds(i,1)) >= bounds(i,2)
        stop_flag = true;
      end
    end
    
    if stop_flag
      break;
    end
      
  end

end