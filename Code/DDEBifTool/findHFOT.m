function hfo_T = findHFOT(fun, Iapp, gti, gpi, T)

  hfo_0 = fzero(@(hfo) fun(0, hfo, 0, gti, gpi), 0.5);

  try
    boundaryConditions = @(ya,yb) ya-hfo_0;

    time = linspace(0, T, 5);
    initialGuess = @(t) hfo_0;
    solinit = bvpinit(time, initialGuess);

    bvpopts = bvpset('AbsTol', 1e-12, 'RelTol', 1e-8, 'Vectorized', true);
    sol = bvp4c(@(t,y) fun(t, y, Iapp, gti, gpi), ...
                  @(ya,yb) boundaryConditions(ya,yb), solinit);
    hfo_T = sol.y(end);
  catch
    warning('Could not solve BVP');
    hfo_T = hfo_0;
  end
              
end