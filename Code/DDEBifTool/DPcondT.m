function [res,J] = DPcondT(point, fun)
  
  Iapp = point.parameter(1);
  gti  = point.parameter(2);
  gpi  = point.parameter(3);
  hfo  = point.parameter(4);
  T    = point.parameter(5);
  
  hfo_T = findHFOT(fun, Iapp, gti, gpi, T);
  
  res = hfo - hfo_T;
  
  J = p_axpy(0, point, []);
  J.parameter(4) = 1;
  
  % Approximate Jacobian using finite difference
  epsi = 0.001;
  
  hfo_TI = findHFOT(fun, Iapp+epsi, gti, gpi, T);
  hfo_Tg = findHFOT(fun, Iapp, gti, gpi+epsi, T);
  hfo_TT = findHFOT(fun, Iapp, gti, gpi, T+epsi);
  
  J.parameter(1) = -(hfo_TI-hfo_T)/epsi;
  J.parameter(3) = -(hfo_Tg-hfo_T)/epsi;
  J.parameter(5) = -(hfo_TT-hfo_T)/epsi;

end