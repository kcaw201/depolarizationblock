function [res,J] = DPcondVar(point)
  
  Iapp = point.parameter(1);
  gti  = point.parameter(2);
  gpi  = point.parameter(3);
  hfo  = point.parameter(4);
  tracker = point.parameter(5);
  
  hfo_inf = findHFO_inf(Iapp, gti, gpi);
  
  res = tracker - (hfo - hfo_inf);
  
  J = p_axpy(0, point, []);
  J.parameter(4) = -1;
  J.parameter(5) = 1;  
  
  % Approximate Jacobian using finite difference
  epsi = 0.001;
  
  hfo_inf_I = findHFO_inf(Iapp+epsi, gti, gpi);
  hfo_inf_g = findHFO_inf(Iapp, gti, gpi+epsi);
  
  J.parameter(1) = -(hfo_inf_I-hfo_inf)/epsi;
  J.parameter(3) = -(hfo_inf_g-hfo_inf)/epsi;

end