function hfo_T = findHFOMSO_T(fun, Iapp, gti, gpi, mso_0, mso_inf, hfo_0)

  debug = false;
  
  time = linspace(0, 1, 5);
  initialGuess = @(t) [mso_0 + t*(mso_inf-mso_inf);
                       hfo_0];
  solinit = bvpinit(time, initialGuess, 200);
  tol = 0.1;
  
  boundaryConditions = @(ya,yb,T) [ya(1)-mso_0;
                                   ya(2)-hfo_0;
                                   yb(1)-(mso_inf-tol)];
                                 
  if debug
    % Do a test trajectory
    [t,y] = ode45(fun, [0, 1000], [mso_0; hfo_0], [], Iapp, gti, gpi);
    plot(t, y);
    hold on;
    line([0,1000], [mso_inf,mso_inf]-tol, 'Color', 'black', 'Linestyle', ':');
    xlabel('Time');ylabel('Profile');
    drawnow; pause;
  end

  sol = bvp4c(@(t,y,T) T*fun(t, y, Iapp, gti, gpi), ...
              boundaryConditions, solinit);
  
  hfo_T = sol.y(2,end);

end