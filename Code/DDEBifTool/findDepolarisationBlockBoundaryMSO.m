% Finding depolaristion block boundary for specific time window using 
% ddebiftool, now with hfo and mso together
% 18.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
eq_branch_name  = 'eq_mso_branch.mat';
LC_branch_name  = 'LC_mso_branch.mat';
LPC_branch_name = 'LPC_mso_branch.mat';
DP_branch_name  = 'DP_mso_branch.mat';
DP_time_branches_name = 'DP_mso_times_branch.mat';
DP_stst_branch_name   = 'DP_mso_stst_new_branch.mat';

load_branches = false;
add_branches  = false;

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

dummy_tau = @()[7];
funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled2,...
  'sys_tau', dummy_tau);

temp = feval(@NowackiSimplifiedScaledSlow2);
fun = temp{2};

T = 500;

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  delay = 0.0; % don't include a delay

  % Parameters
  Iapp = 0.2;
  gti  = 130.0;
  gpi  = 0.35;
  mso  = 0.35;
  hfo  = 0.7;
  
  % Find initial conditions
%   V_fun = @(V) NowackiSimplifiedScaledVoltage(V, Iapp, gti, gpi);
%   V_0  = fzero(V_fun, -0.2);
%   [~,mfo_0,mso_0,hti_0,hfo_0] = V_fun(V_0);
%   init = [V_0, mfo_0, hti_0]';
%   
  init = [-0.2; 0.2; 0.01];
  init = fsolve(@(x) NowackiSimplifiedScaled2(x, [Iapp,gti,gpi,mso,hfo]), init);

  % Use utility functions to get us started
  [stst_branch,suc] = SetupStst(funcs, 'x', init, ...
                           'parameter', [Iapp,gti,gpi,mso,hfo,T,delay], ...
                           'min_bound', [1 0; 2 0; 3 0; 5 0; 6 0], ...
                           'max_bound', [3 1; 4 1], ...
                           'contpar', 5);

  % Continue equilibrium branch
  figure(1);
  stst_branch = br_contn(funcs, stst_branch, 50);
%   stst_branch = br_rvers(stst_branch);
%   stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1); 
  save(eq_branch_name, 'stst_branch');
  
  figure(2); hold on;
  [xm,ym] = df_measr(0, stst_branch, 'stst');
  cla; br_splot(stst_branch, xm, ym);
end

%% Continue solution at Hopf in hfo
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Branch off at Hopf bifurcation');
  ind_hopf = find(nunst_eqs == 2, 1, 'first');
  fprintf('Initial correction of periodic orbits at Hopf:\n');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_hopf, ...
                             'intervals', 20,'degree', 4);

  figure(1);
  LC_branch = br_contn(funcs, LC_branch, 50);
  LC_branch = br_stabl(funcs, LC_branch, 0, 1);
  
  %% Plot stability
  figure(2);
  [xm,ym] = df_measr(0, LC_branch, 'psol');
  ym.col = 'max';
  br_splot(LC_branch, xm, ym)

  ym.col = 'min';
  br_splot(LC_branch, xm, ym);
  
  save(LC_branch_name, 'LC_branch');
end

%% Continue solution at Hopf in mso
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Find orbit at hfo = 0.7');
  ind_switch = find(nunst_eqs == 2, 1, 'first');
  fprintf('Initial correction of periodic orbits at Hopf:\n');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_switch, ...
                             'intervals',20,'degree',4);

  hold on;
  LC_branch = br_contn(funcs, LC_branch, 300);
  LC_branch = br_stabl(funcs, LC_branch, 0, 1);
  
  %% Plot stability
  [xm,ym] = df_measr(0, LC_branch, 'psol');
  ym.col = 'max';
  br_splot(LC_branch, xm, ym)

  ym.col = 'min';
  br_splot(LC_branch, xm, ym);
  
  save(LC_branch_name, 'LC_branch');
end

%% Setup PO fold continuation
if exist(LPC_branch_name, 'file')
  load(LPC_branch_name);
else
  nunst_per = GetStability(LC_branch, 'exclude_trivial', true);
  disp('Continue branch of POfold');
  ind_LPC = find(nunst_per == 2, 1, 'first');
  fprintf('Initial correction of PO fold:\n');
  
  figure;
  funcs.sys_cond = @(pt) DPcondT(pt, fun);
  min_bound = [LC_branch.parameter.min_bound; 5 T];
  [fold_funcs,LPC_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
    'contpar', [3,4,5],...
    'dir', 4, ...
    'min_bound', min_bound);
  
  % %% Find value of T correspond to h - not necessary
  % I_curr    = LPC_branch.point(1).parameter(1);
  % gti_curr  = LPC_branch.point(1).parameter(2);
  % gpi_curr  = LPC_branch.point(1).parameter(3);
  % hfo_curr  = LPC_branch.point(1).parameter(4);
  %
  % T_curr = fzero(@(T) findHFOT(fun, I_curr, gti_curr, gpi_curr, T) ...
  %                              - hfo_curr, 200, optimset('Display', 'iter'));
  %
  % disp('Continuing PO of folds in T');
  
  % LPC_branch.parameter.free = [3 4 5 7 8 9];
  LPC_branch.method.continuation.steplength_growth_factor = 1.0;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);
  
  %% Check that point is on DP boundary
  if debug
    for i = 1:length(LPC_branch.point)
      I_curr    = LPC_branch.point(i).parameter(1);
      gti_curr  = LPC_branch.point(i).parameter(2);
      gpi_curr  = LPC_branch.point(i).parameter(3);
      hfo_curr  = LPC_branch.point(i).parameter(4);
      T_curr    = LPC_branch.point(i).parameter(5);
      
      hfo = findHFOT(fun, I_curr, gti_curr, gpi_curr, T_curr);
      fprintf('Point: %d, Target hfo: %f, actual hfo: %f\n', i, hfo_curr, hfo);
    end
  end
  
  %% Plot branch
  figure;
  [xm,ym] = df_measr(0, LPC_branch, 'psol');
  ym.col = 5;
  br_plot(LPC_branch, xm, ym);
  
  save(LPC_branch_name, 'LPC_branch', 'fold_funcs');
end

%% Now continue along branch with fixed T
if exist(DP_branch_name, 'file')
  load(DP_branch_name);
else
  disp('Continuing PO fold branch with fixed T');
  point = LPC_branch.point(end);
  DP_branch = LPC_branch;
  DP_branch.parameter.free = [1, 3, 4, 7, 8, 9];
  DP_branch.point = [];

  [DP_branch,suc] = correct_ini(fold_funcs, DP_branch, point, ...
                                3, 0.01, 1);

  figure;
  DP_branch.method.continuation.steplength_growth_factor = 1.1;
  DP_branch = br_contn(fold_funcs, DP_branch, 20);
  DP_branch = br_rvers(DP_branch);
  DP_branch = br_contn(fold_funcs, DP_branch, 40);
%   save(DP_branch_name, 'DP_branch');
end

%% Now vary T and recompute branches
if exist(DP_time_branches_name, 'file') && load_branches
  load(DP_time_branches_name);
else
  branch = DP_branch;
  T = branch.point(end).parameter(5);
  Tvals = 500 + (00:50:500);

  max_bound = branch.parameter.max_bound;
  
  DP_time_branches(1) = branch;
  
  for i = 2:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
%     branch = DP_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8, 9];
    branch.point = [];
    branch.parameter.max_bound = [max_bound; 5 Tvals(i)];
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                               5, 1, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    tic;
    branch = br_contn(fold_funcs, branch, 50);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    DP_time_branches(i) = branch;
    save(DP_time_branches_name, 'DP_time_branches', 'fold_funcs');
  end
end

start_ind = 12;
if exist(DP_time_branches_name, 'file') && add_branches
  load(DP_time_branches_name);
  Tvals = 500 + (00:50:500);
  Tvals = [Tvals, 10000];

  for i = start_ind:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
    branch = DP_time_branches(i-1);  

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 3, 4, 7, 8, 9];
    branch.point = [];
    branch = br_replace_bound(branch, 5, Tvals(i), 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                               5, 1, 1);
    
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    tic;
    branch = br_contn(fold_funcs, branch, 10);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);
    
    % Add branch to list
    DP_time_branches(i) = branch;
    save(DP_time_branches_name, 'DP_time_branches', 'fold_funcs');
  end
end

%% Now relax to steady state values of h
if exist(DP_stst_branch_name, 'file')
  load(DP_stst_branch_name);
else
  disp('Continuing PO fold branch for steady state values');
  
  DP_branch = DP_time_branches(end);
  point = DP_branch.point(10);

  fold_funcs = f_replace_PO_sys_cond(fold_funcs, point, @DPcondStSt);
  DP_branch.point = [];
  
  % Rewrite par 5 (used to be for T)
  point.parameter(5) = 0.0;
  [DP_branch,suc] = correct_ini(fold_funcs, DP_branch, point, ...
                                [3], 0.0001, 1);
                              
                                  
  DP_branch = br_replace_bound(DP_branch, 5, -1, 'min');
  DP_branch = br_replace_bound(DP_branch, 5, 1, 'max');
  
  figure;
  DP_branch.method.continuation.steplength_growth_factor = 1.1;
  DP_branch = br_contn(fold_funcs, DP_branch, 100);
  DP_branch = br_rvers(DP_branch);
  DP_branch = br_contn(fold_funcs, DP_branch, 100);
  save(DP_stst_branch_name, 'DP_branch');
end
