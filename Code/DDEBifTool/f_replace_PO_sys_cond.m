% function to replace sys_cond for PO folds
%     - this will only work for this system HACK

% INPUTS:  funcs    - rhs to be solved
%          point    - point on branch (required to get dimension of system and
%          number of parameters)
%          usercond - new sys_cond function taking point as an input
%
% OUTPUT:  funcs - rhs with new user cond
%
% 22.9.2020
% Kyle Wedgwood

function funcs = f_replace_PO_sys_cond(funcs, point, usercond)

  dim  = size(point.profile, 1)-4;   % dimension of original problem
  npar = length(point.parameter)-3;  % number of original system parameters
  beta_ind   = npar+1;               % location of add. parameter beta
  period_ind = npar+2;               % location of copy of period

  get_comp  = @(p,component) extract_from_POfold(p, component, npar);
  relations = [ 0 0 0 0 0 2 0 0 -1];

  funcs.sys_cond = @(point) sys_cond_POfold(point, usercond,...
    dim, beta_ind, period_ind, get_comp, relations);

end
