function [hfo_inf,V] = findHFO_inf(Iapp, gti, gpi)

  V = fzero(@(V) NowackiSimplifiedScaledVoltage(V, Iapp, gti, gpi), -0.8);

  scale   = 100.0;
  V_hfo   = -68.0/scale;
  k_hfo   = -9.7/scale;
  hfo_inf = 1.0/(1.0+exp(-(V-V_hfo)/k_hfo));
  
end