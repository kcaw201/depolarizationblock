% Plot single simulation

function h = plotSingleSimulation(pars, T, x_ind, y_ind, ax)

  if ~exist('ax', 'var')
    fig = figure;
    ax  = axes(fig);
  end
  
  hold(ax, 'on');

  fun = @(V) NowackiSimplifiedScaledVoltage(V, 0.0, pars(2), pars(3));
  V_0 = fzero(fun, -0.8);

  [~,mfo_0,mso_0,hti_0,hfo_0] = fun(V_0);
  X0 = [V_0; mfo_0; mso_0; hti_0; hfo_0];
  
  odeopts = odeset('RelTol', 1e-8, 'AbsTol', 1e-10);

  [t,y] = ode45(@NowackiSimplifiedScaledFull, [0 T], X0, odeopts, pars);
  y = [y,t];
  
  h = plot(y(:,x_ind), y(:,y_ind), 'Color', 'k');

end