% function to replace sys_cond for torus bifurcations 
%     - this will only work for this system HACK

% INPUTS:  funcs    - rhs to be solved
%          point    - point on branch (required to get dimension of system and
%          number of parameters)
%          usercond - new sys_cond function taking point as an input
%
% OUTPUT:  funcs - rhs with new user cond
%
% 22.9.2020
% Kyle Wedgwood

function funcs = f_replace_TR_sys_cond(funcs, point, usercond)

  dim  = size(point.profile, 1)-8;   % dimension of original problem
  npar = length(point.parameter)-2;  % number of original system parameters
  omega_ind  = npar+1;               % location of add. parameter omega
  period_ind = npar+2;               % location of copy of period

  get_comp  = @(p,component) extract_from_tr(p, component);

  funcs.sys_cond = @(point) sys_cond_TorusBif(point, usercond,...
    dim, period_ind, get_comp);

end
