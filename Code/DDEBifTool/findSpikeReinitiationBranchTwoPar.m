% Finding spiking reinitiation branch in (I, gpi) space using
% ddebiftool (to impose additional constraints)
% 19.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
load_branches = false;
save_branches = false;
eq_branch_name  = 'eq_mso_branch.mat';
hopf_branch_name = 'hopf_mso_branch.mat';

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

T = 500.0;

dummy_tau = @()[7];

funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled2, ...
  'sys_tau', dummy_tau);

temp = feval(@NowackiSimplifiedScaledSlow2);
fun = temp{2};

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  delay = 0.0; % don't include a delay
  
  % Parameters
  Iapp = 0.37;
  gti  = 130.0;
  gpi  = 0.2;
  mso  = 0.0;
  hfo  = 0.4;
  
  init = [-0.2766; 0.129; 0.0023];
%   init = [-0.766; 0.129; 0.0023]; % check to see if there are two steady state branches
  par  = [Iapp,gti,gpi,mso,hfo,T,delay];
  
  % Find starting point
  init = fsolve(@(x) funcs.sys_rhs(x,par), init);

  % Use utility functions to get us started
  [stst_branch,suc] = SetupStst(funcs, 'x', init, ...
                                'parameter', par, ...
                                'min_bound', [1 -1; 2 0; 3 -1; 4 -1; 5 0; 6 0], ...
                                'max_bound', [3 1; 4 1; 5 1], ...
                                'contpar', 4, ...
                                'max_step', [4 0.01]);

  % Continue equilibrium branch
  stst_branch = br_contn(funcs, stst_branch, 80);
  stst_branch = br_rvers(stst_branch);
  stst_branch.method.continuation.steplength_growth_factor = 1.0;
%   stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);
  if save_branches
    save(eq_branch_name, 'stst_branch');
  end
  
  %% Plot branch
  [xm,ym] = df_measr(0, stst_branch, 'x');
  figure;
  br_splot(stst_branch, xm, ym);
end

%% Continue Hopf branch
if exist(hopf_branch_name, 'file') && load_branches
  load(hopf_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Continuing Hopf bifurcation');
%   funcs.sys_cond = @SIcondStSt;
  funcs.sys_cond = @(point) SIcondT(point, fun);
  stst_branch.method.point.extra_condition = 1;
  ind_hopf = find(abs(diff(nunst_eqs)) == 2, 1, 'last');
  [hopf_branch,suc] = SetupHopf(funcs, stst_branch, ind_hopf, ...
                                'contpar', [1,3,4,5], ...
                                'dir', [4,5], 'step', 1e-5);
                              
   hopf_branch.method.continuation.steplength_growth_factor = 1.2;
   hopf_branch.method.point.extra_condition = 1;
                              
   hopf_branch = br_contn(funcs, hopf_branch, 100);
   hopf_branch = br_rvers(hopf_branch);
   hopf_branch = br_contn(funcs, hopf_branch, 100);
                              
   %% Plot solution
   figure(99);
   [xm,ym] = df_measr(0, hopf_branch, 'hopf');
   br_plot(hopf_branch, xm, ym);
   xlabel('I');
   ylabel('g_{Nap}');
   y_lim = get(gca, 'YLim');
   ylim([0,y_lim(2)]);

  if save_branches
    save(hopf_branch_name, 'hopf_branch');
  end

end