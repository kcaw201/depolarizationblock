% Plot computed depolarisation block (I, gpi) branches
% 21.8.2020
% Kyle Wedgwood

clear; close all; clc;

addpath('~/Dropbox/MatCont7p2/Systems/');
LPC_branch_name = 'LPC_hfo_times_branch.mat';
TR_branch_name  = 'TR_hfo_times_branch_lower.mat';
PD_branch_name  = 'PD_hfo_times_branch.mat';
fold_mso_branch_name = 'LPC_mso_branch.mat';

load(LPC_branch_name);
load(TR_branch_name);
load(PD_branch_name);

% Plot properties
lw = 5.0;

% Parameter settings
I_ind = 1;
par(I_ind).name   = 'I_{app}';
par(I_ind).units  = 'mA/cm^2';
par(I_ind).limits = [0.03,0.41];
par(I_ind).ticks  = 0:0.1:0.4;

gpi_ind = 3;
par(gpi_ind).name   = 'g_{NaP}';
par(gpi_ind).units  = 'mmho/cm^2';
par(gpi_ind).limits = [0,0.41];
par(gpi_ind).ticks  = 0:0.1:0.4;

T_ind = 5;
par(T_ind).name   = 'T';
par(T_ind).units  = 'ms';
par(T_ind).limits = [0,1000];
par(T_ind).ticks  = 0:200:1000;

fig = figure('Position', [200, 200, 800, 600]);
ax = axes(fig);
hold(ax, 'on');

fprintf('Plotting PO fold branches...\n');
no_branches = length(LPC_time_branches);
cm = flipud(brewermap(no_branches+15, 'Blues'));

h_PO = gobjects(no_branches, 1);

for i = 1:10:length(LPC_time_branches)-1
  branch = LPC_time_branches(i);
  branch_par_val = branch.point(1).parameter(T_ind);
  npts = length(branch.point);
  I_vals = zeros(npts, 1);
  gpi_vals  = zeros(npts, 1);
  for j = 1:npts
    I_vals(j)   = branch.point(j).parameter(I_ind);
    gpi_vals(j) = branch.point(j).parameter(gpi_ind);
  end

  h_PO(i) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
                 'Color', cm(i,:), ...
                 'DisplayName', sprintf('%s = %0.0f', ...
                                        par(T_ind).name, branch_par_val));
end

% Plot steady state branch
load('LPC_hfo_stst_new_branch.mat');
npts = length(LPC_branch.point);
I_vals = zeros(npts, 1);
gpi_vals  = zeros(npts, 1);
for j = 1:npts
  I_vals(j)   = LPC_branch.point(j).parameter(I_ind);
  gpi_vals(j) = LPC_branch.point(j).parameter(gpi_ind);
end

h_PO(i+1) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
                 'Color', cm(i+10,:), ...
                 'DisplayName', 'T = \infty');

fprintf('Plotting PO fold branches...\n');

%% Plot period-doubling branches
fprintf('Plotting period-doubling branches...\n');
no_branches = length(PD_time_branches);
cm = flipud(brewermap(no_branches+1, 'Greens'));

h_PD = gobjects(no_branches, 1);

for i = 1:length(PD_time_branches)
  branch = PD_time_branches(i);
  branch_par_val = branch.point(1).parameter(T_ind);
  npts = length(branch.point);
  I_vals = zeros(npts, 1);
  gpi_vals  = zeros(npts, 1);
  for j = 1:npts
    I_vals(j)   = branch.point(j).parameter(I_ind);
    gpi_vals(j) = branch.point(j).parameter(gpi_ind);
  end

  h_PD(i) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
                 'Color', cm(i,:), ...
                 'DisplayName', sprintf('%s = %0.0f', ...
                                        par(T_ind).name, branch_par_val));
                                      
end

% Change name of steady state branch
set(h_PD(3), 'DisplayName', 'T = \infty');

%% Plot torus bifurcation branches
fprintf('Plotting torus bifurcation branches...\n');
no_branches = length(TR_time_branches);
cm = flipud(brewermap(no_branches+30, 'Reds'));
cm = cm(8:end,:);

h_TR = gobjects(no_branches, 1);

for i = 1:10:length(TR_time_branches)
  branch = TR_time_branches(i);
  branch_par_val = branch.point(1).parameter(T_ind);
  npts = length(branch.point);
  I_vals = zeros(npts, 1);
  gpi_vals  = zeros(npts, 1);
  for j = 1:npts
    I_vals(j)   = branch.point(j).parameter(I_ind);
    gpi_vals(j) = branch.point(j).parameter(gpi_ind);
  end

  h_TR(i) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
                 'Color', cm(i,:), ...
                 'DisplayName', sprintf('%s = %0.0f', ...
                                        par(T_ind).name, branch_par_val));
                                      
  % Plot marker at end of branch
  [I_min,ind] = min(I_vals);
  plot(ax, I_min, gpi_vals(ind), 'Marker', '.', 'Markersize', 50, ...
       'Color', cm(i,:), 'HandleVisibility', 'off');
  
end

% Plot steady state branch
load('TR_hfo_stst_branch.mat');
npts = length(TR_branch.point);
I_vals = zeros(npts, 1);
gpi_vals  = zeros(npts, 1);
for j = 1:npts
  I_vals(j)   = TR_branch.point(j).parameter(I_ind);
  gpi_vals(j) = TR_branch.point(j).parameter(gpi_ind);
end

h_TR(i+1) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
                 'Color', cm(i+10,:), ...
                 'DisplayName', 'T = \infty');
               
% Plot marker at end of branch
[I_min,ind] = min(I_vals);
plot(ax, I_min, gpi_vals(ind), 'Marker', '.', 'Markersize', 50, ...
     'Color', cm(i+10,:), 'HandleVisibility', 'off');
               
% Label axis
xlabel(ax, sprintf('%s (%s)', par(I_ind).name, par(I_ind).units));
ylabel(ax, sprintf('%s (%s)', par(gpi_ind).name, par(gpi_ind).units));
legend(ax);

set(ax, 'XLim', par(I_ind).limits);
set(ax, 'YLim', par(gpi_ind).limits);
set(ax, 'XTick', par(I_ind).ticks, 'YTick', par(gpi_ind).ticks);
set(ax, 'Fontsize', 32);

% Add fold of LC in mso
% load(fold_mso_branch_name);
% npts = length(LPC_branch.point);
% I_vals = zeros(npts, 1);
% gpi_vals  = zeros(npts, 1);
% for j = 1:npts
%   I_vals(j)   = LPC_branch.point(j).parameter(I_ind);
%   gpi_vals(j) = LPC_branch.point(j).parameter(gpi_ind);
% end

% h_TR(i+1) = plot(ax, I_vals, gpi_vals, 'Linewidth', lw, ...
%                  'Color', 'm', ...
%                  'DisplayName', 'T = \infty');
%                
% Plot simulation markers
par(1).Iapp = 0.2;
par(1).gti  = 130.0;
par(1).gpi  = 0.2;
par(1).label = 'A';

par(2).Iapp = 0.20;
par(2).gti  = 130.0;
par(2).gpi  = 0.35;
par(2).label = 'B';

for i = 1:2
  plot(ax, par(i).Iapp, par(i).gpi, 'Marker', 'x', 'Markersize', 20, ...
       'Color', 'black', 'HandleVisibility', 'off', 'Linewidth', 4);
  text(ax, par(i).Iapp-0.025, par(i).gpi+0.002, par(i).label, 'Fontsize', 32); 
end