% Finding depolaristion block boundary for specific time window using
% ddebiftool (to impose additional constraints)
% 18.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
eq_branch_name  = 'eq_hfo_branch.mat';
LC_branch_name  = 'LC_hfo_branch.mat';
LPC_branch_name  = 'LPC_hfo_branch.mat';
LPC_time_branches_name = 'LPC_hfo_times_branch.mat';
LPC_stst_branch_name   = 'LPC_hfo_stst_new_branch.mat';
TR_branch_name  = 'TR_hfo_branch.mat';
TR_time_branches_name = 'TR_hfo_times_branch.mat';
TR_stst_branch_name  = 'TR_hfo_stst_branch.mat';
PD_branch_name = 'PD_hfo_branch.mat';
PD_time_branches_name = 'PD_hfo_times_branch.mat';

load_branches = false;
add_LPC_branches = false;
add_TR_branches  = false;

type = 'PD';

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

dummy_tau = @()[6];
funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled,...
  'sys_tau', dummy_tau);

temp = feval(@NowackiSimplifiedScaledSlow1);
fun = temp{2};

T = 500;

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  delay = 0.0; % don't include a delay

  % Set parameters based on problem type
  switch type
    case 'PD'
      Iapp = 0.1;
      gti  = 130.0;
      gpi  = 0.4;
      hfo  = 0.1;
    
    case 'LPC'
      Iapp = 0.2;
      gti  = 130.0;
      gpi  = 0.2;
      hfo  = 0.2;
      
    case 'TR1'
      Iapp = 0.15;
      gti  = 130.0;
      gpi  = 0.2;
      hfo  = 0.2;
  end
  init = [-0.276; 0.129; 0.56; 0.0023];
  p0 = [Iapp,gti,gpi,hfo,T,delay];
  init = fsolve(@(x) funcs.sys_rhs(x,p0), init);

  % Use utility functions to get us started
  stst_branch = SetupStst(funcs, 'x', init, ...
                           'parameter', p0, ...
                           'min_bound', [1 0; 2 0; 3 0; 4 0; 5 0], ...
                           'max_bound', [3 1; 4 1], ...
                           'contpar', 4);

  % Continue equilibrium branch
  stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);
  save(eq_branch_name, 'stst_branch');
end

%% Continue solution at Hopf
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Branch off at Hopf bifurcation');
  ind_hopf = find(nunst_eqs == 2, 1, 'first');
  fprintf('Initial correction of periodic orbits at Hopf:\n');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_hopf, ...
                             'intervals',20,'degree',4);

  hold on;
  LC_branch = br_contn(funcs, LC_branch, 300);
  LC_branch = br_stabl(funcs, LC_branch, 0, 1);
  nunst_per = GetStability(LC_branch, 'exclude_trivial', true);

  %% Plot stability
  [xm,ym] = df_measr(0, LC_branch, 'psol');
  ym.col = 'max';
  br_splot(LC_branch, xm, ym)

  ym.col = 'min';
  br_splot(LC_branch, xm, ym);

  save(LC_branch_name, 'LC_branch');
end

%% Setup PD fold continuation
if exist(PD_branch_name, 'file') && load_branches
  load(PD_branch_name);
elseif strcmp(type, 'PD')
  disp('Continue branch of period doubling');
  ind_PD = find(diff(nunst_per) == -1, 1, 'last');
  fprintf('Initial correction of period doubling:\n');

  figure;
  funcs.sys_cond = @(pt) DPcondT(pt, fun);
  min_bound = [LC_branch.parameter.min_bound; 5 T];
  
  % %% Find value of T correspond to h - not necessary
  I_curr    = LC_branch.point(ind_PD).parameter(1);
  gti_curr  = LC_branch.point(ind_PD).parameter(2);
  gpi_curr  = LC_branch.point(ind_PD).parameter(3);
  hfo_curr  = LC_branch.point(ind_PD).parameter(4);
  
  T_curr = fzero(@(T) findHFOT(fun, I_curr, gti_curr, gpi_curr, T) ...
                               - hfo_curr, 200, optimset('Display', 'iter'));
  LC_branch.point(ind_PD).parameter(5) = T_curr;
  [PD_funcs,PD_branch,suc] = SetupPeriodDoubling(funcs, LC_branch, ind_PD, ...
    'contpar', [5,4,3],...
    'dir', 4, ...
    'step', 0.0001, ...
    'min_bound', min_bound);
  
  disp('Continuing PO of folds in T');

  % LPC_branch.parameter.free = [3 4 5 7 8 9];
  PD_branch.method.continuation.steplength_growth_factor = 1.2;
%   PD_branch = br_replace_bound(PD_branch, 5, 10000, 'max');
%   PD_branch = br_replace_bound(PD_branch, 5, 0, 'min');
%   PD_branch = br_rvers(PD_branch);
  PD_branch = br_contn(PD_funcs, PD_branch, 100);
  
  %% Plot branch
  figure;
  [xm,ym] = df_measr(0, PD_branch, 'psol');
  ym.col = 3;
  br_plot(PD_branch, xm, ym);

  save(PD_branch_name, 'PD_branch', 'PD_funcs');
  
end

%% Continue period doubling branch for different values of T
% NOTE 7.10.2020 try a different value of I_app to get to steady state
% curve
if exist(PD_time_branches_name, 'file')
  load(PD_time_branches_name);
end
if strcmp(type, 'PD')

  branch = PD_branch;
  T = branch.point(end).parameter(5);
  Tvals = T + (0:500:500);

  point = branch.point(end);

  max_bound = branch.parameter.max_bound;

  PD_branch.parameter.free = [1, 3, 4, 7, 8];
  PD_branch.point = [];

  fprintf('Continuing torus bifurcation in (I,gpi) for T = %.0f\n', T);
  [PD_branch,suc] = correct_ini(PD_funcs, PD_branch, point, ...
                                3, 0.0001, 1);

  figure;
  PD_branch.method.continuation.steplength_growth_factor = 1.2;
  PD_branch = br_remove_bound(PD_branch, 5, 'min');
  PD_branch = br_segcontn(PD_funcs, PD_branch, 3, 50);
  PD_branch = br_rvers(PD_branch);
  PD_branch = br_contn(PD_funcs, PD_branch, 100);

  PD_time_branches(1) = PD_branch;
  save(PD_time_branches_name, 'PD_time_branches', 'PD_funcs');

  for i = 2:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
    branch = PD_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.1);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8];
    branch.point = [];
    branch = br_replace_bound(branch, 5, Tvals(i), 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(PD_funcs, branch, point, ...
                               5, 1, 1);
    figure(100); cla;
    branch = br_contn(PD_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8];
    branch.point = [];

    [branch,suc] = correct_ini(PD_funcs, branch, point, ...
                               1, 0.001, 1);
    figure(100); cla; tic;
    branch = br_remove_bound(branch, 5, 'max');
    branch.parameter.max_step = [];
    branch = br_segcontn(PD_funcs, branch, 50, 10);
    branch = br_rvers(branch);
    branch.parameter.max_step = [1, 0.001; 3, 0.001];
    branch = br_segcontn(PD_funcs, branch, 50, 10);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    PD_time_branches(i) = branch;
    save(PD_time_branches_name, 'PD_time_branches', 'PD_funcs');
  end
end

%% Setup PO fold continuation
if exist(LPC_branch_name, 'file') && load_branches
  load(LPC_branch_name);
elseif strcmp(type, 'LPC')
  disp('Continue branch of POfold');
  ind_LPC = find(nunst_per == 2, 1, 'first');
  fprintf('Initial correction of PO fold:\n');

  figure;
  funcs.sys_cond = @(pt) DPcondT(pt, fun);
  min_bound = [LC_branch.parameter.min_bound; 5 T];
  [fold_funcs,LPC_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
    'contpar', [3,4,5],...
    'dir', 4, ...
    'min_bound', min_bound);

  % %% Find value of T correspond to h - not necessary
  % I_curr    = LPC_branch.point(1).parameter(1);
  % gti_curr  = LPC_branch.point(1).parameter(2);
  % gpi_curr  = LPC_branch.point(1).parameter(3);
  % hfo_curr  = LPC_branch.point(1).parameter(4);
  %
  % T_curr = fzero(@(T) findHFOT(fun, I_curr, gti_curr, gpi_curr, T) ...
  %                              - hfo_curr, 200, optimset('Display', 'iter'));
  %
  % disp('Continuing PO of folds in T');

  % LPC_branch.parameter.free = [3 4 5 7 8 9];
  LPC_branch.method.continuation.steplength_growth_factor = 1.0;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);

  %% Check that point is on DP boundary
  if debug
    for i = 1:length(LPC_branch.point)
      I_curr    = LPC_branch.point(i).parameter(1);
      gti_curr  = LPC_branch.point(i).parameter(2);
      gpi_curr  = LPC_branch.point(i).parameter(3);
      hfo_curr  = LPC_branch.point(i).parameter(4);
      T_curr    = LPC_branch.point(i).parameter(5);

      hfo = findHFOT(fun, I_curr, gti_curr, gpi_curr, T_curr);
      fprintf('Point: %d, Target hfo: %f, actual hfo: %f\n', i, hfo_curr, hfo);
    end
  end

  %% Plot branch
  figure;
  [xm,ym] = df_measr(0, LPC_branch, 'psol');
  ym.col = 5;
  br_plot(LPC_branch, xm, ym);

  save(LPC_branch_name, 'LPC_branch', 'fold_funcs');
end

%% Now continue along branch with fixed T
if exist(LPC_branch_name, 'file') && load_branches
  load(LPC_branch_name);
elseif strcmp(type, 'LPC')
  disp('Continuing PO fold branch with fixed T');
  point = LPC_branch.point(end);
  LPC_branch = LPC_branch;
  LPC_branch.parameter.free = [1, 3, 4, 7, 8, 9];
  LPC_branch.point = [];

  [LPC_branch,suc] = correct_ini(fold_funcs, LPC_branch, point, ...
                                3, 0.01, 1);

  figure;
  LPC_branch.method.continuation.steplength_growth_factor = 1.1;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 20);
  LPC_branch = br_rvers(LPC_branch);
  LPC_branch = br_contn(fold_funcs, LPC_branch, 40);
%   save(LPC_branch_name, 'LPC_branch');
end

%% Now vary T and recompute branches
if exist(LPC_time_branches_name, 'file') && load_branches
  load(LPC_time_branches_name);
elseif strcmp(type, 'LPC')
  branch = LPC_branch;
  T = branch.point(end).parameter(5);
  Tvals = 500 + (00:50:500);

  max_bound = branch.parameter.max_bound;

  LPC_time_branches(1) = branch;

  for i = 2:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
%     branch = LPC_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8, 9];
    branch.point = [];
    branch.parameter.max_bound = [max_bound; 5 Tvals(i)];
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                               5, 1, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    tic;
    branch = br_contn(fold_funcs, branch, 50);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    LPC_time_branches(i) = branch;
    save(LPC_time_branches_name, 'LPC_time_branches', 'fold_funcs');
  end
end

start_LPC_ind = 12;
if exist(LPC_time_branches_name, 'file') && add_LPC_branches
  load(LPC_time_branches_name);
  Tvals = 500 + (00:50:500);
  Tvals = [Tvals, 10000];

  for i = start_LPC_ind:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
    branch = LPC_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 3, 4, 7, 8, 9];
    branch.point = [];
    branch = br_replace_bound(branch, 5, Tvals(i), 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                               5, 1, 1);

    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    tic;
    branch = br_contn(fold_funcs, branch, 10);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    LPC_time_branches(i) = branch;
    save(LPC_time_branches_name, 'LPC_time_branches', 'fold_funcs');
  end
end

%% Now relax to steady state values of h
if exist(LPC_stst_branch_name, 'file')
  load(LPC_stst_branch_name);
else
  disp('Continuing PO fold branch for steady state values');

  LPC_branch = LPC_time_branches(end);
  point = LPC_branch.point(10);

  fold_funcs = f_replace_PO_sys_cond(fold_funcs, point, @DPcondStSt);
  LPC_branch.point = [];

  % Rewrite par 5 (used to be for T)
  point.parameter(5) = 0.0;
  [LPC_branch,suc] = correct_ini(fold_funcs, LPC_branch, point, ...
                                [3], 0.0001, 1);

  LPC_branch = br_replace_bound(LPC_branch, 5, -1, 'min');
  LPC_branch = br_replace_bound(LPC_branch, 5, 1, 'max');

  figure;
  LPC_branch.method.continuation.steplength_growth_factor = 1.1;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);
  LPC_branch = br_rvers(LPC_branch);
  LPC_branch = br_contn(fold_funcs, LPC_branch, 100);
  save(LPC_stst_branch_name, 'LPC_branch');
end

%% Setup torus continuation, start from non-specified value of T and continue
%until T = 500
if exist(TR_branch_name, 'file') && load_branches
  load(TR_branch_name);
elseif strcmp(type, 'TR1')
  disp('Continue branch of POfold');
  ind_TR = find(nunst_per == 2, 1, 'last');
  fprintf('Initial correction of torus bifurcation:\n');

  figure;
  funcs.sys_cond = @(pt) DPcondT(pt, fun);
  min_bound = [LC_branch.parameter.min_bound; 5 T];

  % %% Find value of T correspond to h - not strictly necessary since can
  % be handled by initial correction step
  I_curr    = LC_branch.point(ind_TR).parameter(1);
  gti_curr  = LC_branch.point(ind_TR).parameter(2);
  gpi_curr  = LC_branch.point(ind_TR).parameter(3);
  hfo_curr  = LC_branch.point(ind_TR).parameter(4);

  T_curr = fzero(@(T) findHFOT(fun, I_curr, gti_curr, gpi_curr, T) ...
                               - hfo_curr, 200, optimset('Display', 'iter'));

  LC_branch.point(ind_TR).parameter(5) = T_curr;

  [torus_funcs,TR_branch,suc] = SetupTorusBifurcation(funcs, LC_branch, ind_TR, ...
    'contpar', [5,4,3],...
    'dir', [4], ...
    'step', 0.0001, ...
    'min_bound', min_bound);

  TR_branch.method.continuation.steplength_growth_factor = 1.1;
  TR_branch = br_contn(torus_funcs, TR_branch, 100);

  %% Check that point is on torus boundary
  if debug
    for i = 1:length(TR_branch.point)
      I_curr    = TR_branch.point(i).parameter(1);
      gti_curr  = TR_branch.point(i).parameter(2);
      gpi_curr  = TR_branch.point(i).parameter(3);
      hfo_curr  = TR_branch.point(i).parameter(4);
      T_curr    = TR_branch.point(i).parameter(5);

      hfo = findHFOT(fun, I_curr, gti_curr, gpi_curr, T_curr);
      fprintf('Point: %d, Target hfo: %f, actual hfo: %f\n', i, hfo_curr, hfo);
    end
  end

  %% Plot branch
  figure;
  [xm,ym] = df_measr(0, TR_branch, 'psol');
  ym.col = 5;
  br_plot(TR_branch, xm, ym);

  save(TR_branch_name, 'TR_branch', 'torus_funcs');
end

%% Now vary T and recompute branches
if exist(TR_time_branches_name, 'file') && load_branches
  load(TR_time_branches_name);
elseif strcmp(type, 'TR1')

  branch = TR_branch;
  T = branch.point(end).parameter(5);
  Tvals = T + (0:50:500);

  point = branch.point(end);

  max_bound = branch.parameter.max_bound;

  TR_branch.parameter.free = [1, 3, 4, 7, 8];
  TR_branch.point = [];

  fprintf('Continuing torus bifurcation in (I,gpi) for T = %.0f\n', T);
  [TR_branch,suc] = correct_ini(torus_funcs, TR_branch, point, ...
                                3, 0.01, 1);

  figure;
  TR_branch.method.continuation.steplength_growth_factor = 1.1;
  TR_branch = br_contn(torus_funcs, TR_branch, 200);
  TR_branch = br_rvers(TR_branch);
  TR_branch = br_contn(torus_funcs, TR_branch, 100);

  TR_time_branches(1) = TR_branch;
  save(TR_time_branches_name, 'TR_time_branches', 'torus_funcs');

  for i = 5:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
    branch = TR_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8];
    branch.point = [];
    branch = br_replace_bound(branch, 5, Tvals(i), 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                               5, 1, 1);
    figure(100); cla;
    branch = br_contn(torus_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8];
    branch.point = [];

    [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                               1, 0.01, 1);
    figure(100); cla; tic;
    branch = br_contn(torus_funcs, branch, 200);
    branch = br_rvers(branch);
    branch = br_contn(torus_funcs, branch, 200);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    TR_time_branches(i) = branch;
    save(TR_time_branches_name, 'TR_time_branches', 'torus_funcs');
  end
end

%% Add more branches with new T if required
start_TR_ind = 11;
if add_TR_branches

  T = TR_time_branches(1).point(1).parameter(5);
  Tvals = T + (0:50:500);
  
  for i = start_TR_ind:length(Tvals)
    fprintf('Continuing to T = %0.2f\n', Tvals(i));
    branch = TR_time_branches(i-1);

    % Find point closest to Iapp = 0.3
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.3);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8];
    branch.point = [];
    branch = br_replace_bound(branch, 5, Tvals(i), 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                               5, 1, 1);
    figure(100); cla;
    branch = br_contn(torus_funcs, branch, 50);

    % Now switch back to continuation in (I,gpi,hfo)
    fprintf('Continuing in (Iapp,gpi) for T = %f\n', Tvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 3, 4, 7, 8];
    branch.point = [];

    [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                               1, 0.01, 1);
    figure(100); cla; tic;
    % do try and catch to do small segements
    branch = br_segcontn(torus_funcs, branch, 10, 20);
    branch = br_rvers(branch);
    branch = br_segcontn(torus_funcs, branch, 10, 20);
    toc;

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 3;
    br_plot(branch, xm, ym);

    % Add branch to list
    TR_time_branches(i) = branch;
    save(TR_time_branches_name, 'TR_time_branches', 'torus_funcs');
  end
end

%% Continue to large value of T
if exist(TR_stst_branch_name, 'file') && load_branches
  load(TR_stst_branch_name);
else
  
  if exist('TR_hfo_T1000-10000_branch.mat', 'file')
    load('TR_hfo_T1000-10000_branch.mat');
    branch = br_replace_bound(branch, 5, 20000, 'max');
    branch = br_segcontn(torus_funcs, branch, 10, 10);
  else
    T = 10000;

    fprintf('Continuing to T = %0.2f\n', T);
    branch = TR_time_branches(end);

    % Find point closest to Iapp = 0.5
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.5);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [5, 4, 3, 7, 8];
    branch.point = [];
    branch = br_replace_bound(branch, 5, T, 'max');
    branch.method.continuation.steplength_growth_factor = 1.2;

    branch = br_replace_bound(branch, 1, -10, 'min');
    branch = br_replace_bound(branch, 3, -10, 'min');

    [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                               5, 1, 1);
    
    figure(100); cla;
    branch = br_segcontn(torus_funcs, branch, 100, 10);
    save('TR_hfo_T1000-10000_branch.mat', 'branch', 'torus_funcs');
  end

  % Now switch back to continuation in (I,gpi,hfo)
  point = branch.point(end);
  
  fprintf('Continuing in (Iapp,gpi) at steady state \n');
  
  branch.parameter.free = [1, 3, 4, 7, 8];
  branch.point = [];

  point.parameter(5) = 0.0;
  
  % Replace sys_cond with steady state function
  torus_funcs = f_replace_TR_sys_cond(torus_funcs, point, @DPcondStSt);
  [branch,suc] = correct_ini(torus_funcs, branch, point, ...
                             [1,3,4], 0.00001, 1);
  branch = br_replace_bound(branch, 5, -1, 'min');
  branch = br_replace_bound(branch, 5, 1, 'max');
    
  figure(100); cla; tic;
  % do try and catch to do small segements
  branch = br_segcontn(torus_funcs, branch, 10, 20);
  branch = br_rvers(branch);
  branch = br_segcontn(torus_funcs, branch, 10, 20);
  toc;

  % Plot the branch
  figure(99);
  [xm,ym] = df_measr(0, branch, 'psol');
  ym.col = 3;
  br_plot(branch, xm, ym);

  % Add branch to list
  TR_branch = branch;
  save(TR_stst_branch_name, 'TR_branch', 'torus_funcs');
end
