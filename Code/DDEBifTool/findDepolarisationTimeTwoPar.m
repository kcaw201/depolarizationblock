% Finding depolaristion block time for specific Iapp values using 
% ddebiftool (to impose additional constraints)
% 18.8.2002
% Kyle Wedgwood

clear; close all; clc; debug = false;

% Branch names
eq_branch_name  = 'eq_hfo_branch.mat';
LC_branch_name  = 'LC_hfo_branch.mat';
LPC_branch_name = 'LPC_hfo_branch.mat';
DP_branch_name  = 'DP_time_branches.mat';

addpath(genpath('~/Dropbox/dde_biftool_v3.1.1'));
addpath('~/Dropbox/MatCont7p2/Systems/');

dummy_tau = @()[6];
funcs = set_funcs(...
  'sys_rhs', @NowackiSimplifiedScaled,...
  'sys_tau', dummy_tau);

temp = feval(@NowackiSimplifiedScaledSlow1);
fun = temp{2};

T = 500;

load_branches = true;
save_branches = false;

if exist(eq_branch_name, 'file') && load_branches
  load(eq_branch_name);
else
  delay = 0.0; % don't include a delay

  % Parameters
  Iapp = 0.25;
  gti  = 130.0;
  gpi  = 0.3;
  hfo  = 0.2;
  init = [-0.276; 0.129; 0.56; 0.0023];

  % Use utility functions to get us started
  stst_branch = SetupStst(funcs, 'x', init, ...
                           'parameter', [Iapp,gti,gpi,hfo,T,delay], ...
                           'min_bound', [1 0; 2 0; 3 0; 4 0; 5 0], ...
                           'max_bound', [3 1; 4 1], ...
                           'contpar', 4);

  % Continue equilibrium branch
  stst_branch = br_contn(funcs, stst_branch, 50);
  stst_branch = br_stabl(funcs, stst_branch, 0, 1);
  
  if save_branches
    save(eq_branch_name, 'stst_branch');
  end
end

%% Continue solution at Hopf
if exist(LC_branch_name, 'file') && load_branches
  load(LC_branch_name);
else
  nunst_eqs = GetStability(stst_branch);
  disp('Branch off at Hopf bifurcation');
  ind_hopf = find(nunst_eqs == 2, 1, 'first');
  fprintf('Initial correction of periodic orbits at Hopf:\n');
  [LC_branch,suc] = SetupPsol(funcs, stst_branch, ind_hopf, ...
                             'intervals',20,'degree',4);

  hold on;
  LC_branch = br_contn(funcs, LC_branch, 300);
  LC_branch = br_stabl(funcs, LC_branch, 0, 1);
  
  %% Plot stability
  [xm,ym] = df_measr(0, LC_branch, 'psol');
  ym.col = 'max';
  br_splot(LC_branch, xm, ym)

  ym.col = 'min';
  br_splot(LC_branch, xm, ym);
  
  if save_branches
    save(LC_branch_name, 'LC_branch');
  end
end
% 
% %% Setup PO fold continuation
% if exist(LPC_branch_name, 'var')
%   load(LPC_branch_name);
% else
%   nunst_per = GetStability(LC_branch, 'exclude_trivial', true);
%   disp('Continue branch of POfold');
%   ind_LPC = find(nunst_per == 2, 1, 'first');
%   fprintf('Initial correction of PO fold:\n');
%   
%   figure;
%   funcs.sys_cond = @(pt) DPcondT(pt, fun);
%   min_bound = [LC_branch.parameter.min_bound; 5 0];
%   [fold_funcs,LPC_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
%     'contpar', [3,4,5],...
%     'dir', 4, ...
%     'min_bound', min_bound);
%   
%   % LPC_branch.parameter.free = [3 4 5 7 8 9];
%   LPC_branch.method.continuation.steplength_growth_factor = 1.1;
%   LPC_branch = br_contn(fold_funcs, LPC_branch, 50);
%   LPC_branch = br_rvers(LPC_branch);
%   LPC_branch = br_contn(fold_funcs, LPC_branch, 50);
%   
%   %% Check that point is on DP boundary
%   if debug
%     for i = 1:length(LPC_branch.point)
%       I_curr    = LPC_branch.point(i).parameter(1);
%       gti_curr  = LPC_branch.point(i).parameter(2);
%       gpi_curr  = LPC_branch.point(i).parameter(3);
%       hfo_curr  = LPC_branch.point(i).parameter(4);
%       T_curr    = LPC_branch.point(i).parameter(5);
%       
%       hfo = findHFOT(fun, I_curr, gti_curr, gpi_curr, T_curr);
%       fprintf('Point: %d, Target hfo: %f, actual hfo: %f\n', i, hfo_curr, hfo);
%     end
%   end
%   
%   %% Plot branch
%   figure(99);
%   [xm,ym] = df_measr(0, LPC_branch, 'psol');
%   ym.col = 5;
%   br_plot(LPC_branch, xm, ym);
%   
%   if save_branches
%     save(LPC_branch_name, 'LPC_branch', 'fold_funcs');
%   end
%   
% end

% %% Now cycle through Iapp values (increasing)
% branch = LPC_branch;
% Iapp = branch.point(end).parameter(1);
% gvals = Iapp + (0.0:0.05:0.2);
% 
% max_bound = branch.parameter.max_bound;
% 
% % Follow branch in (I,hfo,T) to next I value
% for i = 2:length(gvals)
%   fprintf('Continuing to I = %0.2f\n', gvals(i));
% %   branch = DP_time_branches(i-1);
%   
%   % Find point closest to gpi = 0.2
%   no_pts = length(branch.point);
%   res = zeros(no_pts, 1);
%   for j = 1:length(branch.point)
%     res(j) = abs(branch.point(j).parameter(3) - 0.2);
%   end
%   [~,ind] = min(res);
%   point = branch.point(ind);
%   
%   branch.parameter.free = [1, 4, 5, 7, 8, 9];
%   branch.point = [];
%   branch.parameter.max_bound = [max_bound; 1 gvals(i); 3 0.4];
%   branch.parameter.min_bound(3,2) = 0.01;
%   
%   [branch,suc] = correct_ini(fold_funcs, branch, point, ...
%                               1, 0.01, 1);
%   figure(100); cla;
%   branch = br_contn(fold_funcs, branch, 50);
%                             
%   % Now switch back to continuation in (gpi,hfo,T)
%   fprintf('Continuing in gpi for I = %0.2f\n', gvals(i));
%   point = branch.point(end);
%   branch.parameter.free = [3, 4, 5, 7, 8, 9];
%   branch.point = [];
%   
%   [branch,suc] = correct_ini(fold_funcs, branch, point, ...
%                               3, 0.01, 1);
%   figure(100); cla;
%   branch = br_contn(fold_funcs, branch, 50);
%   branch = br_rvers(branch);
%   branch = br_contn(fold_funcs, branch, 50);
%   
%   % Plot the branch
%   figure(99);
%   [xm,ym] = df_measr(0, LPC_branch, 'psol');
%   ym.col = 5;
%   br_plot(branch, xm, ym);
%   
%   % Add branch to list
%   DP_time_branches(i) = branch;
% end
% 
% % reverse branch
% DP_time_branches = DP_time_branches(length(DP_time_branches):-1:1);
% 
% %% Now cycle through Iapp values (decreasing)
% Iapp = DP_time_branches(end).point(end).parameter(1);
% gvals = Iapp - (0.0:0.05:0.2);
% 
% min_bound = DP_time_branches(1).parameter.min_bound;
% branch = DP_time_branches(end);
% branch.parameter.max_bound(1,2) = 0.4; % max bound on gpi = 0.4
% 
% % Follow branch in (I,hfo,T) to next I value
% for i = 2:length(gvals)
%   fprintf('Continuing to I = %0.2f\n', gvals(i));
% 
%   % Find point closest to gpi = 0.2
%   no_pts = length(branch.point);
%   res = zeros(no_pts, 1);
%   for j = 1:length(branch.point)
%     res(j) = abs(branch.point(j).parameter(3) - 0.2);
%   end
%   [~,ind] = min(res);
%   point = branch.point(ind);
%   
%   branch.parameter.free = [1, 4, 5, 7, 8, 9];
%   branch.point = [];
%   branch.parameter.min_bound = [min_bound; 1 gvals(i)];
%   
%   [branch,suc] = correct_ini(fold_funcs, branch, point, ...
%                               1, -0.01, 1);
%   figure(100); cla;
%   branch = br_contn(fold_funcs, branch, 50);
%                             
%   % Now switch back to continuation in (gpi,hfo,T)
%   fprintf('Continuing in gpi for I = %0.2f\n', gvals(i));
%   point = branch.point(end);
%   branch.parameter.free = [3, 4, 5, 7, 8, 9];
%   branch.point = [];
%   
%   [branch,suc] = correct_ini(fold_funcs, branch, point, ...
%                               3, 0.01, 1);
%   figure(100); cla;
%   branch = br_contn(fold_funcs, branch, 50);
%   branch = br_rvers(branch);
%   branch = br_contn(fold_funcs, branch, 50);
%   
%   % Plot the branch
%   figure(99);
%   [xm,ym] = df_measr(0, LPC_branch, 'psol');
%   ym.col = 5;
%   br_plot(branch, xm, ym);
%   
%   % Add branch to list
%   DP_time_branches(end+1) = branch;
% end
% 

%% Setup PO fold continuation
if exist(LPC_branch_name, 'var')
  load(LPC_branch_name);
else
  nunst_per = GetStability(LC_branch, 'exclude_trivial', true);
  disp('Continue branch of POfold');
  ind_LPC = find(nunst_per == 2, 1, 'first');
  fprintf('Initial correction of PO fold:\n');
  
  figure;
  funcs.sys_cond = @(pt) DPcondT(pt, fun);
  min_bound = [LC_branch.parameter.min_bound; 1 0.03; 5 0];
  [fold_funcs,LPC_branch,suc] = SetupPOfold(funcs, LC_branch, ind_LPC, ...
    'contpar', [1,4,5],...
    'dir', 4, ...
    'min_bound', min_bound);
  
  % LPC_branch.parameter.free = [3 4 5 7 8 9];
  LPC_branch.method.continuation.steplength_growth_factor = 1.1;
  LPC_branch = br_contn(fold_funcs, LPC_branch, 50);
  LPC_branch = br_rvers(LPC_branch);
  LPC_branch = br_contn(fold_funcs, LPC_branch, 50);
  
   %% Plot branch
  figure(99);
  [xm,ym] = df_measr(0, LPC_branch, 'psol');
  ym.col = 5;
  br_plot(LPC_branch, xm, ym);
  
  if save_branches
    save(LPC_branch_name, 'LPC_branch', 'fold_funcs');
  end
  
end

%% Now cycle through Iapp values (increasing)
if exist(DP_branch_name, 'file') && load_branches
  load(DP_branch_name);
else
  branch = LPC_branch;
  gpi = branch.point(end).parameter(3);
  gvals = gpi + (0.0:0.05:0.1);

  max_bound = branch.parameter.max_bound;
  
  DP_time_branches(1) = branch;

  % Follow branch in (I,hfo,T) to next g value
  for i = 2:length(gvals)
    fprintf('Continuing to gpi = %0.2f\n', gvals(i));
  %   branch = DP_time_branches(i-1);

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [3, 4, 5, 7, 8, 9];
    branch.point = [];
    branch.parameter.max_bound = [max_bound; 3 gvals(i)];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                3, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,hfo,T)
    fprintf('Continuing in Iapp for gpi = %0.2f\n', gvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 4, 5, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    branch = br_contn(fold_funcs, branch, 50);

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 5;
    br_plot(branch, xm, ym);

    % Add branch to list
    DP_time_branches(i) = branch;
  end

  % reverse branch
  DP_time_branches = DP_time_branches(length(DP_time_branches):-1:1);

  %% Now cycle through gpi values (decreasing)
  gpi = DP_time_branches(end).point(end).parameter(3);
  gvals = gpi - (0.0:0.05:0.3);

  min_bound = DP_time_branches(1).parameter.min_bound;
  branch = DP_time_branches(end);
  % branch.parameter.max_bound(1,2) = 0.4; % max bound on gpi = 0.4

  % Follow branch in (I,hfo,T) to next gpi value
  for i = 2:length(gvals)
    fprintf('Continuing to gpi = %0.2f\n', gvals(i));

    % Find point closest to Iapp = 0.2
    no_pts = length(branch.point);
    res = zeros(no_pts, 1);
    for j = 1:length(branch.point)
      res(j) = abs(branch.point(j).parameter(1) - 0.2);
    end
    [~,ind] = min(res);
    point = branch.point(ind);

    branch.parameter.free = [3, 4, 5, 7, 8, 9];
    branch.point = [];
    branch.parameter.min_bound = [min_bound; 3 gvals(i)];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                3, -0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);

    % Now switch back to continuation in (I,hfo,T)
    fprintf('Continuing in Iapp for gpi = %0.2f\n', gvals(i));
    point = branch.point(end);
    branch.parameter.free = [1, 4, 5, 7, 8, 9];
    branch.point = [];

    [branch,suc] = correct_ini(fold_funcs, branch, point, ...
                                1, 0.01, 1);
    figure(100); cla;
    branch = br_contn(fold_funcs, branch, 50);
    branch = br_rvers(branch);
    branch = br_contn(fold_funcs, branch, 50);

    % Plot the branch
    figure(99);
    [xm,ym] = df_measr(0, branch, 'psol');
    ym.col = 5;
    br_plot(branch, xm, ym);

    % Add branch to list
    DP_time_branches(end+1) = branch;
  end

  save(DP_branch_name, 'DP_times_branches', 'fold_funcs');
  
end