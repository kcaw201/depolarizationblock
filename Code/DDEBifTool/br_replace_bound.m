% Replaces parameter bounds, overwrites exiting min/max bound
%
% INPUTS: branch  - branch whose bounds are to be replaced
%         ind     - index of parameter
%         val     - new bound
%         type    - {'min'/'max'} type of bound

% OUTPUT: branch  - branch with new parameter bounds

% 22.9.2020
% Kyle Wedgwood

function branch = br_replace_bound(branch, ind, val, type)

  if strcmp(type, 'max')
    name = 'max_bound';
  elseif strcmp(type, 'min')
    name = 'min_bound';
  else
    disp('Bound type not supported. Must be either min or max.');
  end
  bounds = branch.parameter.(name);
  b_ind = bounds(:,1) == ind;
  bounds(b_ind,:) = [];
  branch.parameter.(name) = [bounds; ind val];
end
