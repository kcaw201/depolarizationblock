function dydt = NowackiSimplifiedScaled2(xx, par)

  V = xx(1,1);
  mfo = xx(2,1);
  hti = xx(3,1);
  
  Iapp = par(1);
  gti  = par(2);
  gpi  = par(3);
  mso  = par(4);
  hfo  = par(5);

  scale=100.0;
  E_i=60.0/scale;
  E_o=-85.0/scale;
  E_l=-65.0/scale;
  
  C=1.0;
  
  V_mti=-37.0/scale;
  V_mpi=-47.0/scale;
  V_mfo=-5.8/scale;
  V_hti=-70.0/scale;
  
  k_mti=5.0/scale;
  k_mpi=3.0/scale;
  k_mfo=11.4/scale;
  k_hti=-7.0/scale;
  
  tau_mfo=1.0;
  tau_hti=1.0;
  
  g_fo=10.0;
  g_so=1.65;
  g_l=0.02;
  
  mti=1.0/(1.0+exp(-(V-V_mti)/k_mti));
  mpi=1.0/(1.0+exp(-(V-V_mpi)/k_mpi));
  mfo_inf=1.0/(1.0+exp(-(V-V_mfo)/k_mfo));
  hti_inf=1.0/(1.0+exp(-(V-V_hti)/k_hti));
  
  I_ti=gti*mti.^3*hti*(V-E_i);
  I_pi=gpi*mpi*(V-E_i);
  I_fo=g_fo*mfo*hfo*(V-E_o);
  I_so=g_so*mso*(V-E_o);
  I_l=g_l*(V-E_l);

  dydt=[-1.0/C*(I_ti+I_pi+I_fo+I_so+I_l-Iapp);
         (mfo_inf-mfo)/tau_mfo;
         (hti_inf-hti)/tau_hti];

end