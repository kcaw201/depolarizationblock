% Plot computed depolarisation block (par, time_to_block) branches
% 21.8.2020
% Kyle Wedgwood

clear; close all; clc;

addpath('~/Dropbox/MatCont7p2/Systems/');
branch_name = 'DP_branches_I_time.mat';

load(branch_name);
no_branches = length(DP_time_branches);

% Plot properties
lw = 8.0;
cm = winter(no_branches);

fig = figure;
ax = axes(fig);
hold(ax, 'on');

par(1).name  = 'I';
par(1).units = 'mA/cm^2';
par(1).limits = [0,1000];
par(1).xticks = 0:0.2:0.4;
par(1).yticks = 0:500:1000;

par(3).name  = 'g_{NaP}';
par(3).units = 'mmho/cm^2';
par(3).limits = [0,0.4];

branch = DP_time_branches(1);
free_par = branch.parameter.free(1);

if free_par == 1
  branch_par = 3;
else
  branch_par = 1;
end

p = par(free_par);
h = gobjects(no_branches, 1);

for i = 1:length(DP_time_branches)
  branch = DP_time_branches(i);
  branch_par_val = branch.point(1).parameter(branch_par);
  npts = length(branch.point);
  par_vals  = zeros(npts, 1);
  time_vals = zeros(npts, 1);
  for j = 1:npts
    par_vals(j)  = branch.point(j).parameter(free_par);
    time_vals(j) = branch.point(j).parameter(5);
  end
  
  h(i) = plot(ax, par_vals, time_vals, 'Linewidth', lw, ...
              'Color', cm(i,:), ...
              'DisplayName', sprintf('%s = %0.2f', ...
                                     par(branch_par).name, branch_par_val));
end

% Label axis
xlabel(ax, sprintf('%s (%s)', p.name, p.units));
ylabel(ax, 'Time (ms)');
legend(ax);

set(ax, 'YLim', p.limits);
set(ax, 'XTick', p.xticks, 'YTick', p.yticks);
set(ax, 'Fontsize', 32);