function [res,J] = SIcondStSt(point)
  
  Iapp = point.parameter(1);
  gti  = point.parameter(2);
  gpi  = point.parameter(3);
  mso  = point.parameter(4);
  hfo  = point.parameter(5);
  
  % First do mso
  mso_inf = findMSO_inf(Iapp, gti, gpi);
  res(1) = mso - mso_inf;
  
  J(1) = p_axpy(0, point, []);
  J(1).parameter(4) = 1;
  
  % Approximate Jacobian using finite difference
  epsi = 0.001;
  
  mso_inf_I = findMSO_inf(Iapp+epsi, gti, gpi);
  mso_inf_g = findMSO_inf(Iapp, gti, gpi+epsi);
  J(1).parameter(1) = -(mso_inf_I-mso_inf)/epsi;
  J(1).parameter(3) = -(mso_inf_g-mso_inf)/epsi;
  
  % Then do hfo
  hfo_0 = findHFO_inf(0.0, gti, gpi);
  res(2) = hfo - hfo_0;
  
  J(2) = p_axpy(0, point, []);
  J(2).parameter(5) = 1;
  
  hfo_0_g = findHFO_inf(0, gti, gpi+epsi);
  J(2).parameter(3) = -(hfo_0_g-hfo_0)/epsi;
  
end