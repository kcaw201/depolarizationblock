function [res,mfo_inf,mso_inf,hti_inf,hfo_inf] = ...
  NowackiSimplifiedScaledVoltage(V, par_Iapp, par_gti, par_gpi)

  scale=100.0;
  E_i=60.0/scale;
  E_o=-85.0/scale;
  E_l=-65.0/scale;

  V_mti=-37.0/scale;
  V_mpi=-47.0/scale;
  V_mfo=-5.8/scale;
  V_mso=-30.0/scale;
  V_hti=-70.0/scale;
  V_hfo=-68.0/scale;

  k_mti=5.0/scale;
  k_mpi=3.0/scale;
  k_mfo=11.4/scale;
  k_mso=10.0/scale;
  k_hti=-7.0/scale;
  k_hfo=-9.7/scale;

  g_fo=10.0;
  g_so=1.65;
  g_l=0.02;

  mti_inf=1.0/(1.0+exp(-(V-V_mti)/k_mti));
  mpi_inf=1.0/(1.0+exp(-(V-V_mpi)/k_mpi));
  mfo_inf=1.0/(1.0+exp(-(V-V_mfo)/k_mfo));
  mso_inf=1.0/(1.0+exp(-(V-V_mso)/k_mso));
  hti_inf=1.0/(1.0+exp(-(V-V_hti)/k_hti));
  hfo_inf=1.0/(1.0+exp(-(V-V_hfo)/k_hfo));

  I_ti=par_gti*mti_inf.^3*hti_inf*(V-E_i);
  I_pi=par_gpi*mpi_inf*(V-E_i);
  I_fo=g_fo*mfo_inf*hfo_inf*(V-E_o);
  I_so=g_so*mso_inf*(V-E_o);
  I_l=g_l*(V-E_l);

  res = (I_ti+I_pi+I_fo+I_so+I_l-par_Iapp);

end