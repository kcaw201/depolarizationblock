function [mso_inf,V] = findMSO_inf(Iapp, gti, gpi)

  V = fzero(@(V) NowackiSimplifiedScaledVoltage(V, Iapp, gti, gpi), -0.8);

  scale   = 100.0;
  V_mso   = -30.0/scale;
  k_mso   = 10.0/scale;
  mso_inf = 1.0/(1.0+exp(-(V-V_mso)/k_mso));
  
end