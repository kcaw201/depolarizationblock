% Plot results from firing map function
% Kyle Wedgwood
% 2.9.2019

clear; close all;

g_NaT = 130;

filename = sprintf( 'heatmap_gnat_%d_original.mat', g_NaT);
load( filename);

digits = regexp( filename, '[0-9]', 'match');
g_NaT_str = '';

for digit = digits
  g_NaT_str = [ g_NaT_str, digit{1}];
end

g_NaT = str2double( g_NaT_str);

fig = figure( 'Position', [400,400,1000,800]);
ax  = axes( fig);

h = imagesc( I_app, g_NaP, no_APs);
xlabel( 'Current density (AU)');
ylabel( 'g_{NaP} (AU)');
title( ax, sprintf( 'g_{NaT} = %d (AU)', g_NaT));

cbar = colorbar( ax);
cbar.Label.String = 'APs/500 ms';
cbar.Label.FontSize = 32;
set( cbar, 'YTick', 0:20:100)

set( ax, 'YDir', 'normal');
set( ax, 'XTick', 0:0.1:0.4);
set( ax, 'YTick', 0:0.1:0.4);
set( ax, 'Fontsize', 32);

% For original system plots
gradient_thresh = 0.008;
AP_thresh = 0.20;

% % For transientK_only plots
% gradient_thresh = 0.008;
% AP_thresh = 0.20;

[DP_flag,contour_DP] = ComputeDPContour( final_point, gradient_thresh, g_NaP, g_NaT, I_app);
[spike_flag,contour_spike] = ComputeSpikingContour( max_dv, AP_thresh, g_NaP, I_app);

hold (ax);

DP_boundary = plot( ax, contour_DP(1,:), contour_DP(2,:), 'Linestyle', '-',  ...
  'Linewidth', 6.0, 'Color', 'red');
spiking_boundary = plot( ax, contour_spike(1,:), contour_spike(2,:), 'Linestyle', '-', ...
  'Linewidth', 6.0, 'Color', 'green');